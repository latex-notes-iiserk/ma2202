\documentclass[11pt]{scrartcl}

\usepackage[linear]{handout}
\usepackage[normalem]{ulem}
\usepackage{fouridx, mathrsfs}
\usepackage{dsfont}

\newcommand{\E}{\mathcal{E}}
\DeclareMathOperator{\Exp}{E}
\DeclareMathOperator{\Var}{Var}
\DeclareMathOperator{\Cov}{Cov}

\newcommand{\sumx}{\sum_{x: P(X=x)>0}}
\newcommand{\sumy}{\sum_{y: P(Y=y)>0}}

\graphicspath{{./images/}}

\title{Assignment 06: Solutions}
\subtitle{MA2202: Probability I}
\author{Piyush Kumar Singh \\ \texttt{22MS027} \\[-2pt] Group - B}
\date{\today}

\begin{document}
\maketitle

\begin{exercisebox}
    \begin{exercise}[5 Points]
        Let $X \sim N(0, 1)$. Show that the MGF of $X$ is given by $M_X(t) = e^{t^2/2}$.
    \end{exercise}
\end{exercisebox}

\begin{proof}
    Since we are given that $X$ is a standard normal random variable, we have
    \begin{equation*}
        f_X(t) = \frac{1}{\sqrt{2 \pi}} e^{-x^2 / 2}
    \end{equation*}
    We have to find the expectation of random variable $e^{t X}$,
    \begin{equation*}
        M_X(t) = \Exp(e^{t X}) = \frac{1}{\sqrt{2 \pi}} \int\limits_{-\infty}^{\infty} e^{t x} e^{-x^2/2} \dd{x}
    \end{equation*}
    After combining the exponents and completing the square, we have
    \begin{gather*}
        M_X(t) = \frac{1}{\sqrt{2 \pi}} \int\limits_{-\infty}^{\infty} \exp(-\qty(\frac{x}{\sqrt{2}} - \frac{t}{\sqrt{2}})^2) e^{t^2/2} \dd{x}
        \shortintertext{Since $e^{t^2/2}$ is independent of $x$, so we can factor it out. Also, after a u-substitution for $u = x - t$, we have}
        M_X(t) = e^{t^2/2} \frac{1}{\sqrt{2 \pi}} \int\limits_{-\infty}^{\infty} e^{-u^2/2} \dd{u}
        \shortintertext{Observe that the integrand is just the PDF of standard normal, so we have}
        M_X(t) = e^{t^2/2}
    \end{gather*}
\end{proof}


\begin{exercisebox}
    \begin{exercise}[10 Points]
        Let $X_1, X_2, \dots, X_n$ be iid discrete random variables with common PMF $f$. Let $m \le n$ be a positive integer and let $t_1, t_2, \dots, t_m$ be $m$ distinct points in the range of $X_1$. Determine the probability
        \begin{equation*}
            P\qty(\qty{X_1 , X_2 , \dots, X_n} \subseteq \qty{t_1 , t_2 , \dots, t_m})
        \end{equation*}
    \end{exercise}
\end{exercisebox}

\begin{solution}
    Using the fact that order statistics maps all the $n$ random variables, the asked quantity is related to the order statistics as follows
    \begin{equation*}
        P\qty(\qty{X_1 , X_2 , \dots, X_n} \subseteq \qty{t_1 , t_2 , \dots, t_m}) = P\qty(\qty{X_{(1)} , X_{(2)} , \dots, X_{(n)}} \subseteq \qty{t_1 , t_2 , \dots, t_m})
    \end{equation*}
    We must distribute these $m$ distinct points in $n$ order statistics. Since $m \le n$, so there will be repetition, say $t_i$ points is used $r_i$ times, then we have $0 \le r_i \le n ~\forall i \in \qty{1, 2, \dots, m}$. Also, there is a constraint in this distribution \ie
    \begin{equation*}
        \sum_{i = 1}^{m} r_i = n \qquad \qquad 0 \le r_i \le n ~\forall i \in \qty{1, 2, \dots, m}
    \end{equation*}
    Now, using the PMF of joint discrete order statistics,
    \begin{gather*}
        P\qty(\qty{X_{(1)} , X_{(2)} , \dots, X_{(n)}} \subseteq \qty{t_1 , t_2 , \dots, t_m}) = \sum_{\substack{r_1 + r_2 + \cdots + r_m = n \\ 0 \le r_1, r_2, \dots, r_m \le n}} \frac{n!}{r_1! r_2! \cdots r_m!} f(t_1)^{r_1} f(t_2)^{r_2} \cdots f(t_m)^{r_m}
        \intertext{We can observe that RHS is just a multinomial expansion,}
        P\qty(\qty{X_{(1)} , X_{(2)} , \dots, X_{(n)}} \subseteq \qty{t_1 , t_2 , \dots, t_m}) = \qty(f(t_1) + f(t_2) + \cdots + f(t_m))^n \\[1.2ex]
        \boxed{P\qty(\qty{X_1 , X_2 , \dots, X_n} \subseteq \qty{t_1 , t_2 , \dots, t_m}) = \qty(f(t_1) + f(t_2) + \cdots + f(t_m))^n}
    \end{gather*}
\end{solution}


\begin{exercisebox}
    \begin{exercise}[5 Points]
        Show that for $x \in (0, 1)$ and $j \in {1, 2, \dots, n}$, we have
        \begin{equation*}
            \sum_{r = j}^{n} \binom{n}{r} x^r (1-x)^{n-r} = n \binom{n-1}{j-1} \int\limits_{0}^{x} t^{j-1} (1-t)^{n-j} \dd{x}
        \end{equation*}
    \end{exercise}
\end{exercisebox}

\begin{proof}
    Consider a random variable $X \sim \mathrm{Uniform}(0, 1)$, so we have the following PDF and CDF
    \begin{equation*}
        f_X(t) = 
        \begin{cases}
            1 & t\in(0,1) \\
            0 & \text{otherwise}
        \end{cases} 
        \qquad \qquad \qquad 
        F_X(t) = \int\limits_{-\infty{}{}}^{t} f_X(x) \dd{x} = 
        \begin{cases}
            0 & t \le 0 \\
            t & t\in (0,1) \\
            1 & t \ge 1
        \end{cases} 
    \end{equation*}
    Consider $n$ iid Uniform$(0, 1)$ random variable $X_1, X_2, \dots, X_n$. We know that PDF and CDF of $j^{\text{th}}$ order statistics is given by
    \begin{gather*}
        f_{X_{(j)}}(t) = n \binom{n-1}{j-1} F(t)^{j-1} (1 - F(t))^{n-j} f(t) \\
        F_{X_{(j)}}(t) = \sum_{r = j}^{n} \binom{n}{r} F(t)^r (1 - F(t))^{n-r}
    \end{gather*}
    where $f(t)$ is the common PDF and $F(t)$ is the common CDF. And by the definition of PDF, for $x \in (0, 1)$ we have
    \begin{equation*}
        F_{X_{(j)}}(x) = \int\limits_{-\infty}^{x} f_{X_{(j)}}(t) \dd{t} = \int\limits_{0}^{x} f_{X_{(j)}}(t) \dd{t}
    \end{equation*}
    Now, if we substitute the common PDF and CDF of Uniform$(0, 1)$ in the last equation, we have
    \begin{equation*}
        \sum_{r = j}^{n} \binom{n}{r} x^r (1-x)^{n-r} = n \binom{n-1}{j-1} \int\limits_{0}^{x} t^{j-1} (1-t)^{n-j} \dd{x}
    \end{equation*}
\end{proof}
\newpage
\begin{exercisebox}
    \begin{exercise}[10 Points]
        Let there be a coin such that the probability of obtaining a head when the coin is tossed is $p$. Let $H_n$ and $T_n$ denote the number of heads and tails in $n$ tosses of the coin. Given $\epsilon > 0$, show that
        \begin{equation*}
            P\qty(2 p - 1 - \epsilon \le \frac{1}{n}(H_n - T_n) \le 2 p - 1 + \epsilon) \to 1 ~ \text{as} ~ n \to \infty.
        \end{equation*}
    \end{exercise}
\end{exercisebox}

\begin{proof}
    Since we have $n$ tosses, let us define two sets of $n$ iid random variables for each $i^{\text{th}}$ toss as
    \begin{equation*}
        h_i \sim \text{Bernoulli}(p) \implies h_i =
        \begin{cases}
            1 & \text{if } i^{\text{th}} \text{ toss is head} \\
            0 & \text{if } i^{\text{th}} \text{ toss is tail}
        \end{cases}
        \qquad
        t_i \sim \text{Bernoulli}(1-p) \implies t_i =
        \begin{cases}
            1 & \text{if } i^{\text{th}} \text{ toss is tail} \\
            0 & \text{if } i^{\text{th}} \text{ toss is head}
        \end{cases} 
    \end{equation*}
    With these iid random variables, let's define the corresponding Binomial random variable.
    \begin{equation*}
        H_n := \sum_{i=1}^{n} h_i \implies H_n \sim \text{Binomial}(n, p) \qquad \qquad \qquad T_n := \sum_{i=1}^{n} t_i \implies T_n \sim \text{Binomial}(n, 1-p)
    \end{equation*}
    Using this terminology, we have sample mean as follows
    \begin{equation*}
        \bar{h}_n = \frac{\sum\limits_{i=1}^{n} h_i}{n} = \frac{H_n}{n} \qquad \qquad \qquad \bar{t}_n = \frac{\sum\limits_{i=1}^{n} t_i}{n} = \frac{T_n}{n}
    \end{equation*}
    Since $H_n$ is the random variable that denotes the number of heads in $n$ tosses so, by the definition of $h_i$'s and $t_i$'s, we have the following relation,
    \begin{equation*}
        H_n = n - T_n
    \end{equation*}
    This equation represents that in $n$ tosses, the sum of the number of heads and tails is constant. Using this relation, we have,
    \begin{equation*}
        \frac{H_n - T_n}{n} = 2\frac{H_n}{n} - 1 \quad \implies \quad \bar{h}_n = \frac{H_n}{n} = \frac{H_n - T_n}{2 n} + \half
    \end{equation*}
    \begin{corollary*}[The Weak Law of Large Numbers]
        Let $X_1, X_2, \dots, X_n$ be a set of $n$ iid random variables with expectation $\mu$ and variance $\sigma^2$. Then for all $\epsilon > 0$, we have
        \begin{equation*}
            \lim_{n \to \infty} P\qty(\abs{\Bar{X}_n - \mu} < \epsilon) = 1
        \end{equation*}
    \end{corollary*}
    Now, using this corollary for random variables $h_i$'s, we have
    \begin{gather*}
        \lim_{n \to \infty} P\qty(\abs{\Bar{h}_n - p} < \epsilon/2) = 1 \\
        \lim_{n \to \infty} P\qty(p - \epsilon/2 < \Bar{h}_n < p + \epsilon/2) = 1 \\
        \lim_{n \to \infty} P\qty(p - \epsilon/2 < \frac{H_n}{n} < p + \epsilon/2) = 1 \\
        \lim_{n \to \infty} P\qty(p - \epsilon/2 < \frac{H_n - T_n}{2 n} + \half < p + \epsilon/2) = 1 \\
        \lim_{n \to \infty} P\qty(2 p - 1 - \epsilon < \frac{H_n - T_n}{n} < 2 p - 1  + \epsilon) = 1
    \end{gather*}
\end{proof}


\begin{exercisebox}
    \begin{exercise}[20 Points]
        Show that if two states of a Markov chain are in the same communication class, either both of them are recurrent or both of them are transient.
    \end{exercise}
\end{exercisebox}

\begin{proof}
    Let's start with the definition of recurrent and transient states.
    \begin{definition*}[Classification of State]
        Consider a Markov chain $\qty{X_n: n \in \N_0}$\footnotemark on finite state space $S$ with transition matrix $\P$. Let $X_0 = i$ for some $i \in S$, define revisiting probability of state $i$ as
        \begin{equation*}
            f_i := P\qty(X_n = i \text{ for some $n \in \N$} | X_0 = i)
        \end{equation*}
        \begin{description}[nolistsep, noitemsep]
            \item[Recurrent State:] A state $i \in S$ such that $f_i = 1$.
            \item[Transient State:] A state $i \in S$ such that $f_i < 1$.
        \end{description}
    \end{definition*}
    \footnotetext{Notation: $\N_0 = \qty{0, 1, 2, \dots}$ and $\N = \qty{1, 2, 3, \dots}$}
    \begin{lemma}\label{lemma:lemma1}
        Every state is either recurrent or transient.
    \end{lemma}
    \begin{Proof}
        For $k \in \N$ consider the random event
        \begin{equation*}
            B_k = \qty{X_n = i \text{ for at least $k$ different values of } n \in \N}.
        \end{equation*}
        Then, $P(B_k) = f^k_i$. Also, $B_1 \supseteq B_2 \supseteq \dots$. It follows that
        \begin{equation*}
            P\qty(X_n = i \text{ for infinitely many } n) = P\qty(\bigcap_{k=1}^{\infty} B_k) = \lim_{k\to \infty} P\qty(B_k) = \lim_{k \to \infty} f^k_i = 
            \begin{cases}
                1, & \text{if } f_i = 1 \\
                0, & \text{if } f_i < 1
            \end{cases}
        \end{equation*}
        It follows that state $i$ is recurrent if $f_i = 1$ and transient if $f_i < 1$.
    \end{Proof}

    \begin{lemma}\label{lemma:lemma2}
        Let $i \in S$ be a state.
        \begin{itemize}[noitemsep,nolistsep]
            \item The state $i$ is recurrent, iff $\sum_{k=1}^{\infty} p_{ii}^{(n)} = \infty$. 
            \item The state $i$ is transient, iff $\sum_{k=1}^{\infty} p_{ii}^{(n)} < \infty$.
        \end{itemize}
    \end{lemma}
    \begin{Proof}
        Consider the random variable $V_i$, which counts the number of returns of the Markov chain to state i.
        \begin{equation*}
            V_i := \sum_{n = 1}^{\infty} \mathds{1}_{\qty{X_n = i}}
        \end{equation*}
        Note that the random variable $V_i$ can take the value $+\infty$.
        \begin{equation*}
            P\qty(V_i \ge k) = P(B_k) = f^k_i, \quad \forall k \in \N.
        \end{equation*}
        Thus, the expectation of $V_i$ can be computed as follows:
        \begin{equation*}
            \Exp[V_i] = \sum_{k = 1}^{\infty} P(V_i > k) = \sum_{k = 1}^{\infty} P(V_i \ge k+1) = \sum_{k = 1}^{\infty} f_i^{k+1}
        \end{equation*}
        On the other hand,
        \begin{equation*}
            \Exp[V_i] = \Exp\qty[\sum_{n = 1}^{\infty} \mathds{1}_{\qty{X_n = i}}] = \sum_{n = 1}^{\infty} \Exp\qty[\mathds{1}_{\qty{X_n = i}}] = \sum_{n = 1}^{\infty} p_{ii}^{(n)}
        \end{equation*}
        \textsc{Case 1:} Assume that state $i$ is recurrent. Then, $f_i = 1$ which implies that
        \begin{equation*}
            \sum_{k = 1}^{\infty} f_i^{k+1} = \infty \qquad \implies \qquad \sum_{n = 1}^{\infty} p_{ii}^{(n)} = \infty
        \end{equation*}
        \textsc{Case 2:} Assume that state $i$ is transient. Then, $f_i < 1$ which implies that
        \begin{equation*}
            \sum_{k = 1}^{\infty} f_i^{k+1} < \infty \qquad \implies \qquad \sum_{n = 1}^{\infty} p_{ii}^{(n)} < \infty
        \end{equation*}
    \end{Proof}
    % Now, we will prove that if two states are communicating and one of them is recurrent, then the other state is recurrent as well. Using this result, we can prove the other part as well \ie if one of them is a transient state, then the other state is transient as well.
    Using \cref{lemma:lemma1}, we can say that it is enough to prove the following statements,
    \begin{itemize}[$\odot$, noitemsep]
        \item If $i \in S$ is a recurrent state and $i \leftrightarrow j$, then $j$ is also a recurrent state.
        \item If $i \in S$ is a transient state and $i \leftrightarrow j$, then $j$ is also a transient state.
    \end{itemize}
    \begin{lemma}\label{lemma:lemma3}
        If $i \in S$ is a recurrent state and $i \leftrightarrow j$, then $j$ is also a recurrent state.
    \end{lemma}
    \begin{Proof}
        Let $i$ be a recurrent state and $i \leftrightarrow j$. It follows that there exists $s, r \in \N_0$ with $p^{(s)}_{ij} > 0$ and $p^{(r)}_{ji} > 0$. For all $n \in \N$ it holds that
        \begin{equation*}
            p_{jj}^{(r+n+s)} \ge p_{ji}^{(r)} p_{ii}^{(n)} p_{ij}^{(s)}
        \end{equation*}
        This follows since the left side of the preceding is the probability of going from $j$ to $j$ in $r + n + s$ steps. In contrast, the right side is the probability of going from $j$ to $j$ in $r + n + s$ steps via a path that goes from $j$ to $i$ in $r$ steps, then from $i$ to $i$ in an additional $n$ steps, then from $i$ to $j$ in an additional $s$ steps. \\[1.2ex]
        Therefore,
        \begin{equation*}
            \sum_{n = 1}^{\infty} p_{jj}^{(r+n+s)} \ge p_{ji}^{(r)} p_{ij}^{(s)} \sum_{n = 1}^{\infty} p_{ii}^{(n)} = \infty
        \end{equation*}
        Using \cref{lemma:lemma2}, we conclude that $j$ is a recurrent state.
    \end{Proof}
    \begin{lemma}
        If $i \in S$ is a transient state and $i \leftrightarrow j$, then $j$ is also a transient state.
    \end{lemma}
    \begin{Proof}
        Suppose not $j$ is not a transient state, then by \cref{lemma:lemma1}, $j$ is a recurrent state. So by \cref{lemma:lemma3} $i$ is also a recurrent state, which is a contradiction.
    \end{Proof}
\end{proof}


\end{document}