\documentclass[11pt]{scrartcl}

\usepackage{fouridx}
\usepackage[normalem]{ulem}
\usepackage[linear]{handout}

\newcommand{\E}{\mathcal{E}}
\DeclareMathOperator{\Exp}{E}
\DeclareMathOperator{\Var}{Var}
\graphicspath{{./images/}}

\title{Assignment 03: Solutions}
\subtitle{MA2202: Probability I}
\author{Piyush Kumar Singh \\ \texttt{22MS027} \\[-2pt] Group - B}
\date{\today}

\begin{document}
\maketitle

\begin{exercisebox}
    \begin{exercise}[2+2+6 Points]
        Solve the following exercises:
        \setlist{nosep}
        \begin{enumerate}[(a)]
            \item Let \((\Omega, \E, P)\) be a probability space and let \(A \subset \Omega\). Define \(X: \Omega \to \R\) by
                  \[
                      X(\omega) :=
                      \begin{cases}
                          1 & \text{if}~\omega \in A \\
                          0 & \text{otherwise}.
                      \end{cases}
                  \]
                  In which of the following cases is \(X\) a random variable? Justify your answer!\\
                  \begin{inparaenum}[(i)] \item \(A \in \E\) \item \(A \notin \E\) \end{inparaenum}

            \item Let \((\Omega, \E, P)\) be a probability space with \(\Omega = \qty{1, 2, 3, 4, 5}\) and \(\E = \qty{\emptyset, \Omega, \qty{1}, \qty{2, 3, 4, 5}}\). Define \(X: \Omega \to \R\) by
                  \[
                      X(\omega) := \omega + 1
                  \]
                  for all $\omega \in \Omega$. Is $X$ a random variable? Justify your answer!
        \end{enumerate}
    \end{exercise}
\end{exercisebox}

\begin{solution}
    For this problem, we require the definition of a random variable and the axioms of probability.
    % \setlist{nosep}
    \begin{enumerate}[(a)]
        \item In this random variable, we have \(X(\Omega) = \qty{0, 1}\). If we consider two cases as,
              \begin{enumerate}[(i)]
                  \item Let \(A \in \E\). \\[1.2ex]
                        Let \(k \ge 1\) so \(X^{-1}(-\infty, k] = \Omega \in \E\).\\
                        Let \(0 \le k < 1\) so \(X^{-1}(-\infty, k] = A^{c} \in \E\).\\
                        Let \(k < 0\) so \(X^{-1}(-\infty, k] = \emptyset \in \E\).\\[1.2ex]
                        So, we can conclude that \(X\) is a random variable of probability space \(\Omega\).

                  \item Let \(A \notin \E\). \\[1.2ex]
                        Consider \(X^{-1}(\qty{1}) = A \notin \E\).\\[1.2ex]
                        So, we can conclude that \(X\) is not a random variable.
              \end{enumerate}

        \item The given random variable is defined as \(X: \Omega \to \R\), by \(X(\omega) = \omega + 1\), so the range is \(X(\Omega) = \qty{2, 3, 4, 5, 6}\). If there exists an interval \(I\) such that \(X^{-1}(I) \notin \E\), then \(X\) is not a random variable for the given sample space. Consider an interval \((-\infty, 3]\), and we calculate the pre-image of this interval under the map \(X\). We have,
              \begin{equation*}
                  X^{-1}(-\infty, 3] = \qty{1, 2} \notin \E
              \end{equation*}
              So, we can conclude that \(X\) is not a random variable.
    \end{enumerate}
\end{solution}


\begin{exercisebox}
    \begin{exercise}[10 Points]
        Let $X$ be a random variable defined on $\qty{1, 2, \dots, 10}$ with PMF $f(x) = ax+b$ and expectation 7. Find $a$ and $b$.
    \end{exercise}
\end{exercisebox}

\begin{solution}
    Since \(X\) is a random variable defined on \(\qty{1, 2, \dots, 10}\), then \(X\) is a discrete random variable. From the definition of a discrete random variable, we have,
    \begin{equation*}
        \sum_{x: P(X=x)>0} P(X=x) = 1
    \end{equation*}
    So, with the given PMF, we have,
    \begin{equation*}
        \sum_{i = 1}^{10} f(i) = 1 \quad \implies \quad 55 a + 10 b = 1
    \end{equation*}
    Recalling the definition of expectation of a discrete random variable, we have,
    \begin{equation*}
        \sum_{i = 1}^{10} i f(i) = 7 \quad \implies \quad 385 a + 55 b = 7
    \end{equation*}
    After solving these two linear equations, we have,
    \begin{equation*}
        \boxed{a = \frac{1}{55}, \qquad b = 0}
    \end{equation*}
\end{solution}


\begin{exercisebox}
    \begin{exercise}[10 Points]
        For what value of the constant $c$, the real valued function $f: \R \to \R$ given by
        \[
            f(x) := \frac{c}{1 + (x-\theta)^2}
        \]
        Where \(\theta\) is a real parameter, is a PDF of random variable $X$?
    \end{exercise}
\end{exercisebox}

\begin{solution}
    If \(f\) is a PDF, so by definition we have,
    \begin{gather*}
        \int_{-\infty}^{\infty} f(x) \dd{x} = 1
        \intertext{Substituting \(f\) in the equation}
        c \int_{-\infty}^{\infty} \frac{1}{1 + (x-\theta)^2} \dd{x} = 1
        \intertext{let, \(u = x - \theta\) and after change of variable, we have}
        c \int_{-\infty}^{\infty} \frac{1}{1 + u^2} \dd{u} = 1 \\
        c \eval[\tan^{-1} u|_{-\infty}^{\infty} = 1 \\
        c \pi = 1 \\
        \boxed{c = \frac{1}{\pi}}
    \end{gather*}
\end{solution}


\begin{exercisebox}
    \begin{exercise}[4+4 Points]
        Let $p \in [0, 1]$, $a, b \in \R$ with $a > b$ and let $X$ be a random variable such that
        \[
            P (X = a) = p ~\text{and}~ P (X = b) = 1 - p.
        \]
        Find the expectation and variance of \(\frac{X-b}{b - a}\).
    \end{exercise}
\end{exercisebox}

\begin{solution}
    Let us define a function \(g\) of the given random variable \(X\) as \(g: \R \to \R\) by
    \begin{equation*}
        g(X) := \frac{X - b}{b - a}
    \end{equation*}
    We must calculate \(\Exp[g]\) and \(\Var[g]\). For a discrete random variable, we have
    \begin{equation*}
        \mu := \Exp[g(X)] = \sum_{x: P(X=x)>0}\!\!\! g(x) P(x) \qquad \& \qquad \Var[g(X)] = \Exp[(g(X) - \mu)^2]
    \end{equation*}
    Now, if we calculate the expectation value, we have,
    \begin{gather*}
        \Exp[g(X)] = g(X = a) P(X = a) + g(X = b) P(X = b) \\
        = (-1) (p) + (0) (1 - p) \\
        \boxed{\Exp[g(X)] = -p}
    \end{gather*}
    Now, if we calculate the variance, we have,
    \begin{align*}
        \Var[g(X)]         & = \sum_{x: P(X=x)>0} P(X = x) (g(X = x) - \mu)^2            \\
                           & = P(X = a) (g(X = a) - \mu)^2 + P(X = b) (g(X = b) - \mu)^2 \\
                           & = p (-1 + p)^2 + (1 - p) (p)^2                              \\
                           & = p (1 - p) [(1 - p) + p]                                   \\
        \Aboxed{\Var[g(X)] & = p (1 - p)}
    \end{align*}
\end{solution}


\begin{exercisebox}
    \begin{exercise}[4+4+4 Points]
        A bag contains five coins, two of which are made of gold and the rest are made of silver. Consider the random experiment in which the coins are drawn out of the bag randomly, one after another, without replacement. Let $X$ denote the number of draws until the last gold coin is drawn. Find the PMF, the CDF and the expectation of the random variable $X$.
    \end{exercise}
\end{exercisebox}

\begin{solution}
    We can represent the bag as \(B = \qty{G, G, S, S, S}\). If we draw five coins one after another without replacement, only ten permutations are possible.\\
    % \begin{equation*}
    %     \frac{5!}{2! \cdot 3!} = 10
    % \end{equation*}
    For this random experiment, we can draw both gold coins at a minimum of two draws and a maximum of five draws. So, the random variable maps as \(X(\Omega) = \qty{2, 3, 4, 5}\). We can find the pre-images of these mappings as,
    \begin{align*}
        X^{-1}(\qty{2}) & = \qty{GG}                         \\
        X^{-1}(\qty{3}) & = \qty{SGG, GSG}                   \\
        X^{-1}(\qty{4}) & = \qty{SSGG, SGSG, GSSG}           \\
        X^{-1}(\qty{5}) & = \qty{SSSGG, GSSSG, SSGSG, SGSSG}
    \end{align*}

    Now, we can define PMF for all \(x \in \qty{2, 3, 4, 5}\) as follows,
    \begin{align*}\allowdisplaybreaks
        P(X = 2) & = P(\qty{GG}) = \frac{2}{5} \cdot \frac{1}{4} = \frac{1}{10}                                                                                                        \\
        P(X = 3) & = P(\qty{SGG, GSG}) = \frac{3}{5} \cdot \frac{2}{4} \cdot \frac{1}{3} + \frac{2}{5} \cdot \frac{3}{4} \cdot \frac{1}{3} = \frac{1}{10} + \frac{1}{10} = \frac{1}{5} \\
        P(X = 4) & = P(\qty{SSGG, SGSG, GSSG}) = \frac{1}{10} + \frac{1}{10} + \frac{1}{10} = \frac{3}{10}                                                                             \\
        P(X = 5) & = P(\qty{SSSGG, GSSSG, SSGSG, SGSSG}) = \frac{1}{10} + \frac{1}{10} + \frac{1}{10} + \frac{1}{10} = \frac{2}{5}
    \end{align*}
    Say, we define PMF \(\Lambda: \R \to [0, 1]\) by
    \begin{equation*}
        \Lambda(X = x) :=
        \begin{cases}
            \frac{x-1}{10}, & x \in \qty{2, 3, 4, 5} \\[1.5ex]
            0,              & \text{otherwise}
        \end{cases}
    \end{equation*}
    Using this definition, we can define \(F_{X}: \R \to [0,1]\) by
    \begin{equation*}
        F_{X}(x) := P(X \le x) =
        \begin{cases}
            0,            & x < 2       \\
            \frac{1}{10}, & 2 \le x < 3 \\[1.6ex]
            \frac{3}{10}, & 3 \le x < 4 \\[1.6ex]
            \frac{3}{5},  & 4 \le x < 5 \\[1.6ex]
            1,            & x \ge 5
        \end{cases}
    \end{equation*}
    We can calculate the expectation value of this random variable as,
    \begin{align*}
        \Exp[X]         & = \sum_{x: P(X=x)>0} x P(X = x)                                                           \\
                        & = 2 \cdot \frac{1}{10} + 3 \cdot \frac{1}{5} + 4 \cdot \frac{3}{10} + 5 \cdot \frac{2}{5} \\
                        & = \frac{1}{5} + \frac{3}{5} + \frac{6}{5} + \frac{10}{5}                                  \\
                        & = \frac{20}{5}                                                                            \\
        \Aboxed{\Exp[X] & = 4}
    \end{align*}
\end{solution}

\end{document}