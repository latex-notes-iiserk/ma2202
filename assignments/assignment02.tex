\documentclass[12pt]{scrartcl}

\usepackage{fouridx}
\usepackage[normalem]{ulem}
\usepackage[linear]{handout}

\newcommand{\E}{\mathcal{E}}
\graphicspath{{./images/}}

\title{Assignment 02: Solutions}
\subtitle{MA2202: Probability I}
\author{Piyush Kumar Singh \\ \texttt{22MS027} \\[-2pt] Group - B}
\date{\today}

\begin{document}
\maketitle

\begin{exercisebox}
    \begin{exercise}[10 Points]
        There are $n$ boxes numbered $1, 2, \dots, n$, among which the $r$\textsuperscript{th} box contains $r - 1$ white cubes and $n - r$ red cubes. Suppose we choose a box at random and we remove two cubes from it, one after another, without replacement.
        \setlist{nolistsep}
        \begin{enumerate}[(a), noitemsep]
            \item Find the probability of the second cube being red.
            \item Find the probability of the second cube being red, given that the first cube is red.
        \end{enumerate}
    \end{exercise}
\end{exercisebox}

\begin{solution}
    The total number of cubes in any given box is \(n - 1\). Probability of choosing \(r\)\textsuperscript{th} box at random is \(1/n\).
    \begin{itemize}[noitemsep]
        \item \(B_r\): choosing \(r\)\textsuperscript{th} box,
        \item \(R\): removing two cubes, with the second cube being red.
    \end{itemize}
    \begin{enumerate}[(a)]
        \item For this part we must find \(P(R)\), and using the \vocab{total probability lemma}, we have
              \begin{equation*}
                  P(R) = \sum_{r = 1}^{n} P(B_r) P(R | B_r)
              \end{equation*}
              We can remove two cubes from \(r\)\textsuperscript{th} box in two ways so that the second cube is red. We can remove `a white and a red cube' or `two red cubes.'
              \begin{equation*}
                  P(R | B_r) = \frac{r - 1}{n - 1} \cdot \frac{n - r}{n - 2} + \frac{n - r}{n - 1} \cdot \frac{n - r - 1}{n - 2}
              \end{equation*}
              We know \(P(B_r) = 1/n\), so we have \(P(B)\) as follows
              \begin{align*}
                  P(B) & = \sum_{r = 1}^{n} \frac{(r-1)(n-r)}{n(n-1)(n-2)} + \frac{(n-r)(n-r-1)}{n(n-1)(n-2)} \\
                       & = \sum_{r = 1}^{n} \frac{n - r}{n(n-1)}                                              \\
                       & = \sum_{r = 1}^{n} \frac{1}{n-1} - \frac{r}{n(n-1)} \displaybreak[3]                 \\
                       & = \frac{n}{n-1} - \frac{n+1}{2(n-1)}                                                 \\
                       & = \frac{2 n - n - 1}{2(n - 1)}                                                       \\
                       & = \boxed{\frac{1}{2}}
              \end{align*}
        \item \(R^{(1)}\): first cube being red, and \(R^{(2)}\): second cube being red. We must find
              \begin{equation*}
                  P(R^{(2)} | R^{(1)}) = \frac{P(R^{(1)} \cap R^{(2)})}{P(R^{(1)})}
              \end{equation*}
              \(P(R^{(1)} \cap R^{(2)})\): the probability of removing two cubes with both being red.
              \begin{align*}
                  P(R^{(1)} \cap R^{(2)}) & = \sum_{r = 1}^{n} \frac{1}{n} \cdot \frac{n-r}{n-1} \cdot \frac{n - r - 1}{n - 2} \\
                                          & = \frac{1}{n(n-1)(n-2)} \sum_{r = 1}^{n} (n-r)^2 - (n-r)                           \\
                                          & = \frac{1}{n(n-1)(n-2)} \qty[\sum_{i = 0}^{n-1} i^2 - \sum_{i = 0}^{n-1} i]        \\
                                          & = \frac{1}{n(n-1)(n-2)} \qty[\frac{n(n-1)(2 n - 1)}{6} - \frac{n(n-1)}{2}]         \\
                                          & = \frac{1}{3}
              \end{align*}
              \(P(R^{(1)})\): the probability of removing a red cube first, we have
              \begin{align*}
                  P(R^{(1)}) & = \sum_{r = 1}^{n} \frac{1}{n} \cdot \frac{n - r}{n-1} \\
                             & = \frac{1}{2}
              \end{align*}
              So, the required probability is given by,
              \begin{equation*}
                  P(R^{(2)} | R^{(1)}) = \frac{P(R^{(1)} \cap R^{(2)})}{P(R^{(1)})} = \frac{1/3}{1/2} = \boxed{\frac{2}{3}}
              \end{equation*}
    \end{enumerate}
\end{solution}


\begin{exercisebox}
    \begin{exercise}[10 Points]
        Let $(\Omega, \E, P)$ be a probability space and let $A_1 , A_2, \dots, A_n \in \E$ with $P\qty(\bigcap_{i = 1}^{n} A_i) \ne 0$. Show that
        \begin{equation*}
            P\qty(\bigcap_{i = 1}^{n} A_i) = P(A_1) P(A_2 | A_1) P(A_3 | A_2 \cap A_1) \cdots P\qty(A_n | A_{n-1} \cap \cdots \cap A_1).
        \end{equation*}
    \end{exercise}
\end{exercisebox}

\begin{proof}
    We can prove this statement using induction.\\
    {\sffamily \uline{Base Case:}} For \(n = 1\), the statement holds trivially. For \(n = 2\), by definition of conditional probability, we have
    \begin{equation*}
        P(A_2 | A_1) = \frac{P(A_1 \cap A_2)}{P(A_1)} \qquad \implies \qquad P(A_1) P(A_2 | A_1) = P(A_1 \cap A_2)
    \end{equation*}
    {\sffamily \uline{Induction Case:}} Consider the statement holds true for \(n = k\), we have to show that it holds for \(n = k+1\) as well.
    \begin{equation*}
        P\qty(\bigcap_{i = 1}^{k} A_i) = P(A_1) P(A_2 | A_1) P(A_3 | A_2 \cap A_1) \cdots P\qty(A_k | A_{k-1} \cap \cdots \cap A_1).
    \end{equation*}
    Observe, \(\bigcap_{i = 1}^{k+1} A_i = \qty(\bigcap_{i = 1}^{k} A_i) \cap A_{k+1}\). Now, we can use the base case on these two sets,
    \begin{equation*}
        P\qty(\bigcap_{i = 1}^{k+1} A_i) = P\qty(\bigcap_{i = 1}^{k} A_i) P(A_{k+1} | A_{k} \cap \cdots \cap A_1)
    \end{equation*}
    Using the induction hypothesis,
    \begin{equation*}
        P\qty(\bigcap_{i = 1}^{k+1} A_i) = P(A_1) P(A_2 | A_1) P(A_3 | A_2 \cap A_1) \cdots P\qty(A_{k+1} | A_{k} \cap \cdots \cap A_1).
    \end{equation*}
    This completes the proof.
\end{proof}


\begin{exercisebox}
    \begin{exercise}[10 Points]
        Let $(\Omega, \E, P)$ be a probability space and let $A_1, A_2, \dots \in \E$ be pairwise mutually exclusive. Let $A = \bigcup_{n = 1}^{\infty} A_n$ and let $B \in \E$ with $P(B) \ne 0$. Show that
        \begin{equation*}
            P(A | B) = \sum_{n=1}^{\infty} P(A_n | B).
        \end{equation*}
    \end{exercise}
\end{exercisebox}

\begin{proof}
    From the axioms of probability, let \(E_i \in \E ~ \forall ~ i\)
    \begin{equation*}
        P\qty(\bigcup_{i = 1}^{\infty} E_{i}) = \sum_{i = 1}^{\infty} P(E_i).
    \end{equation*}
    Using this axiom, we can prove the given statement,
    \begin{align*}
        P(A | B) & = P\qty(\bigcup_{n = 1}^{\infty} A_n \bigg| B)                  \\
                 & = \frac{P\qty(\qty(\bigcup_{n = 1}^{\infty} A_n) \cap B)}{P(B)} \\
                 & = \frac{P\qty(\bigcup_{n = 1}^{\infty} (A_n \cap B))}{P(B)}     \\
                 & = \sum_{n = 1}^{\infty} \frac{P(A_n \cap B)}{P(B)}              \\
                 & = \sum_{n = 1}^{\infty} P(A_n | B)
    \end{align*}
\end{proof}


\begin{exercisebox}
    \begin{exercise}[9+3+3 points]
        We are familiar with the famous Monty Hall problem. Now suppose, instead of 3 doors, there are $n$ doors, only one among which has a prize behind it.
        \setlist{nolistsep}
        \begin{enumerate}[(a), noitemsep]
            \item Find the probability of winning upon switching given that Monty opens $k$ doors. Will switching benefit you?
            \item Find the probability of winning upon switching, given that Monty opens maximum number of doors. Will switching benefit you?
            \item Find the probability of winning upon switching, given that Monty opens no doors. Will switching benefit you?
        \end{enumerate}
    \end{exercise}
\end{exercisebox}

\begin{solution}
    We have \(n\) doors, and only one contains a prize. So, the probability that our initial guess is correct is \(1/n\). Let us define the following events:
    \begin{itemize}[noitemsep]
        \item \(I\): initial guess is right.
        \item \(I^c\): initial guess is wrong.
        \item \(O\): Monty opens \(k\) empty doors.
        \item \(S\): second guess is correct (given first was wrong).
    \end{itemize}
    To decide whether switching is better or not, we must compare \(P(I | O)\) and \(P(S | O)\). In general, \(0 \le k \le n-2\) as Monty has to leave my initial guess and at least one door for switching. Observe that \(P(O | I) = P(O | S) = P(O) = 1\) as in any case Monty opens \(k\) doors.
    \begin{enumerate}[(a)]
        \item By conditional probability,
              \begin{equation*}
                  P(I | O) = \frac{P(O | I) P(I)}{P(O)} = P(I) = \frac{1}{n}.
              \end{equation*}
              The second probability is,
              \begin{equation*}
                  P(S | O) = \frac{P(O | S) P(S)}{P(O)} = P(S) = \underbrace{\frac{n-1}{n}}_{P(I^c)} \cdot \overbrace{\frac{1}{n - k - 1}}^{P(\text{selecting correct door})}
              \end{equation*}
              Observe, \((n-1)/(n-k-1) \ge 1\) for \(0 \le k \le n-2\). Then
              \begin{equation*}
                  P(S | O) \ge P(I | O)
              \end{equation*}
              So, if Monty at least opens one door, then switching is beneficial.

        \item The Maximum number of doors which Monty can open is \(n-2\). \\
              So \(P(S | O) = (n-1)/n > 1/n\). Then switching is better.

        \item If Monty opens no door, then \(k = 0\), implying \(P(S | O) = P(I | O)\), so there is no statistical benefit in switching.
    \end{enumerate}
\end{solution}


\begin{exercisebox}
    \begin{exercise}[10 Points]
        Dropping two points uniformly at random on $[0, 1]$, the unit interval is divided into three segments. Find the probability that the three segments obtained in this way form a triangle.
    \end{exercise}
\end{exercisebox}

\begin{solution}
    Consider two points \(x, y \in [0, 1]\) chosen randomly. We can create a triangle in possible cases:\\
    {\sffamily \uline{Case 1:}} \(x < y\). \\
    For this configuration, the sides of the triangle will be $x$, $y - x$, \(1 - y\). Since these three line segments form a triangle, so they must satisfy the triangle inequality, which gives us the following inequality (or conditions) on \(x, y\)
    \setlist{nosep}
    \begin{itemize}
        \item \(y > \half\)
        \item \(x < \half\)
        \item \(y < x + \half\)
    \end{itemize}
    \vspace{3ex}
    \noindent {\sffamily \uline{Case 2:}} \(y < x\). \\
    For this configuration, the sides of the triangle will be $y$, $x - y$, \(1 - x\). Since these three line segments form a triangle, so they must satisfy the triangle inequality, which gives us the following inequality (or conditions) on \(x, y\)
    \setlist{nosep}
    \begin{itemize}
        \item \(x > \half\)
        \item \(y < \half\)
        \item \(y > x - \half\)
    \end{itemize}
    \begin{figure}[H]
        \centering
        \includegraphics[width = 0.55\textwidth]{ass-02-q5.png}
        \caption{Region colored in light blue corresponds to the values of \(x, y\) which forms a valid triangle. And the blue square represents all possible \(x, y\).}
        \label{fig:triangle}
    \end{figure}
    With these two cases and \cref{fig:triangle}, we can quantify the probability as follows:
    \begin{equation*}
        P(\text{forming a triangle}) = \frac{\text{Area of light blue regions}}{\text{Area of blue square}}
    \end{equation*}
    So the area of the square = \(1 \times 1\) and area of the two small triangles = \(2 \cdot \half\qty(\half)\qty(\half) = \frac{1}{4}\).
    \begin{equation*}
        P(\text{forming a triangle}) = \frac{1/4}{1} = \frac{1}{4}
    \end{equation*}
\end{solution}

\end{document}