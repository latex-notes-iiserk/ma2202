\documentclass[11pt]{scrartcl}

\usepackage{fouridx}
\usepackage[normalem]{ulem}
\usepackage[linear]{handout}

\newcommand{\E}{\mathcal{E}}
\DeclareMathOperator{\Exp}{E}
\DeclareMathOperator{\Var}{Var}
\DeclareMathOperator{\Cov}{Cov}

\newcommand{\sumx}{\sum_{x: P(X=x)>0}}
\newcommand{\sumy}{\sum_{y: P(Y=y)>0}}

\graphicspath{{./images/}}

\title{Assignment 04: Solutions}
\subtitle{MA2202: Probability I}
\author{Piyush Kumar Singh \\ \texttt{22MS027} \\[-2pt] Group - B}
\date{\today}

\begin{document}
\maketitle

\begin{exercisebox}
    \begin{exercise}[5+5 Points]
        Let $X, Y$, and $Z$ be iid positive random variables defined over the same sample space. Show that
        \begin{equation*}
            \Exp\qty(\frac{2 X + Z}{X + Y + Z})
        \end{equation*}
        exits and determine its value.
    \end{exercise}
\end{exercisebox}

\begin{solution}
    Since, \(X, Y\), and \(Z\) are iid positive random variables. So we have
    \begin{itemize}[$\cdot$, noitemsep]
        \item \(X, Y, Z\) are mutually independent.
        \item \(X, Y, Z\) are identically distributed \ie
              \begin{equation}\label{eq:cdf-eq}
                  F_X = F_Y = F_Z
              \end{equation}
    \end{itemize}
    \uline{Discrete random variable:} Since we have equal CDF for these given random variables. We can observe that the corresponding PMF of these given random variables is equal. So we have,
    \begin{equation} \label{eq:pmf-eq}
        P_X = P_Y = P_Z
    \end{equation}
    \begin{claim}
        \(\Exp\qty(\frac{A}{X + Y + Z})\) exists for all \(A \in \qty{X, Y, Z}\).
    \end{claim}
    \begin{Proof}
        Since these all are positive random variables so, we have,
        \begin{equation} \label{eq:positive-ineq}
            x < x + y + z \qquad \quad \forall ~ x, y, z \in (0, \infty)
        \end{equation}
        Using this inequality, we have,
        \begin{gather*}
            \frac{x}{x+y+z} < 1 \\
            \sum_{\substack{\textcolor{white}{x}\\x,y,z : P(X=x, Y=y, Z=z) > 0}} \hspace{-40pt} \frac{x}{x+y+z} P(X=x, Y=y, Z=z) < \sum_{\substack{\textcolor{white}{x}\\x,y,z : P(X=x, Y=y, Z=z) > 0}} \hspace{-40pt} P(X=x, Y=y, Z=z) = 1 \\
            \Exp\qty(\frac{X}{X+Y+Z}) < 1
        \end{gather*}
        % Observe this summation is running over the set $\qty{x,y,z ~|~ P(X=x, Y=y, Z=z) > 0}$. \\
        Hence, we can conclude that \(\Exp(X/(X+Y+Z))\) exists. Similarly \(\Exp(Y/(X+Y+Z)), ~\Exp(Z/X+Y+Z)\) exists.
    \end{Proof}

    \begin{claim}
        \(
        \Exp\qty(\frac{X}{X+Y+Z}) = \Exp\qty(\frac{Y}{X+Y+Z}) = \Exp\qty(\frac{Z}{X+Y+Z})
        \)
    \end{claim}
    \begin{Proof}
        By definition, we have
        \begin{gather*}
            \Exp\qty(\frac{X}{X+Y+Z}) = \sum_{\substack{\textcolor{white}{x}\\x,y,z : P(X=x, Y=y, Z=z) > 0}} \hspace{-40pt} \frac{x}{x+y+z} P(X=x, Y=y, Z=z)
            \shortintertext{Since these are iid random variables,}
            \Exp\qty(\frac{X}{X+Y+Z}) = \sum_{\substack{\textcolor{white}{x}\\x,y,z : P(X=x, Y=y, Z=z) > 0}} \hspace{-40pt} \frac{x}{x+y+z} P_X(X=x) P_Y(Y=y) P_Z(Z=z)
            \shortintertext{Since the individual PMF's are equal and \(\qty{x : P_X(X = x) > 0} = \qty{y : P_Y(Y = y)> 0} = \qty{z : P_Z(Z = z)> 0}\). We can change the variables \(x \leftrightarrow y\) and the sum is not changed, we have}
            \Exp\qty(\frac{X}{X+Y+Z}) = \sum_{\substack{\textcolor{white}{x}\\x,y,z : P(X=x, Y=y, Z=z) > 0}} \hspace{-40pt} \frac{y}{y+x+z} P_X(X=y) P_Y(Y=x) P_Z(Z=z)
            \shortintertext{Using equality of PMF's (see \cref{eq:pmf-eq})}
            \Exp\qty(\frac{X}{X+Y+Z}) = \sum_{\substack{\textcolor{white}{x}\\x,y,z : P(X=x, Y=y, Z=z) > 0}} \hspace{-40pt} \frac{y}{y+x+z} P_Y(Y=y) P_X(X=x) P_Z(Z=z) \\
            \Exp\qty(\frac{X}{X+Y+Z}) = \sum_{\substack{\textcolor{white}{x}\\x,y,z : P(X=x, Y=y, Z=z) > 0}} \hspace{-40pt} \frac{y}{y+x+z} P(X=x, Y=y, Z=z) \\
            \Exp\qty(\frac{X}{X+Y+Z}) = \Exp\qty(\frac{Y}{X+Y+Z})
        \end{gather*}
        We can prove another equality similarly.
    \end{Proof}

    \noindent \uline{Continuous random variable:} Since these are iid random variables, we have equal CDFs. For every continuous random variable, we have countable infinite choices for pdf. Say a function \(f_X : \R \to \R_{\ge 0}\) is PDF of random variable \(X\), so we have
    \begin{equation*}
        F_X(x) = \int\limits_{-\infty}^{x} f_X(t) \dd{t} \qquad \forall~ x \in \R
    \end{equation*}
    For a given continuous random variable, CDF is the defining property. Since we have the choice to choose PDF for random variables, we can select the PDF of \(X\) to be the PDF of \(Y\) and \(Z\) as well. So, by the construction, we have
    \begin{equation*}
        f_X = f_Y = f_Z
    \end{equation*}
    This selection of PDFs satisfies the condition in \cref{eq:cdf-eq}.

    \begin{claim}
        \(\Exp\qty(\frac{A}{X + Y + Z})\) exists for all \(A \in \qty{X, Y, Z}\).
    \end{claim}
    \begin{Proof}
        Since these random variables are positive \cref{eq:positive-ineq} holds. Using this, we have
        \begin{gather*}
            \frac{x}{x+y+z} < 1 \qquad \forall~ x,y,z \in (0, \infty) \\
            \frac{x}{x+y+z} f_{X, Y, Z} (x, y, z) < f_{X, Y, Z} (x, y, z) \\
            \int\limits_{0}^{\infty}\int\limits_{0}^{\infty}\int\limits_{0}^{\infty} \frac{x}{x+y+z} f_{X, Y, Z} (x, y, z) \dd{x}\dd{y}\dd{z} < \int\limits_{0}^{\infty}\int\limits_{0}^{\infty}\int\limits_{0}^{\infty} f_{X, Y, Z} (x, y, z) \dd{x}\dd{y}\dd{z} \displaybreak[3] \\
            \Exp\qty(\frac{X}{X+Y+Z}) < 1
        \end{gather*}
        Hence, we can conclude that \(\Exp(X/(X+Y+Z))\) exists. Similarly \(\Exp(Y/(X+Y+Z)), ~\Exp(Z/X+Y+Z)\) exists.
    \end{Proof}

    \begin{claim}
        \(
        \Exp\qty(\frac{X}{X+Y+Z}) = \Exp\qty(\frac{Y}{X+Y+Z}) = \Exp\qty(\frac{Z}{X+Y+Z})
        \)
    \end{claim}
    \begin{Proof}
        By definition, we have
        \begin{gather*}
            \Exp\qty(\frac{X}{X+Y+Z}) = \int\limits_{0}^{\infty}\int\limits_{0}^{\infty}\int\limits_{0}^{\infty} \frac{x}{x+y+z} f_{X, Y, Z} (x, y, z) \dd{x}\dd{y}\dd{z}
            \shortintertext{Since these are iid random variables}
            \Exp\qty(\frac{X}{X+Y+Z}) = \int\limits_{0}^{\infty}\int\limits_{0}^{\infty}\int\limits_{0}^{\infty} \frac{x}{x+y+z} f_X(x) f_Y(y) f_Z(z) \dd{x}\dd{y}\dd{z}
            \shortintertext{We have chosen the same PDF for all three random variables. So, we can change the variables \(x \leftrightarrow y\), keeping the integrals unchanged.}
            \Exp\qty(\frac{X}{X+Y+Z}) = \int\limits_{0}^{\infty}\int\limits_{0}^{\infty}\int\limits_{0}^{\infty} \frac{y}{y+x+z} f_X(y) f_Y(x) f_Z(z) \dd{y}\dd{x}\dd{z}
            \shortintertext{Using the fact that we have the same PDFs,}
            \Exp\qty(\frac{X}{X+Y+Z}) = \int\limits_{0}^{\infty}\int\limits_{0}^{\infty}\int\limits_{0}^{\infty} \frac{y}{y+x+z} f_Y(y) f_X(x) f_Z(z) \dd{y}\dd{x}\dd{z}
            \shortintertext{Using the property that these three random variables are independent,}
            \Exp\qty(\frac{X}{X+Y+Z}) = \int\limits_{0}^{\infty}\int\limits_{0}^{\infty}\int\limits_{0}^{\infty} \frac{y}{x+y+z} f_{X, Y, Z} (x, y, z) \dd{x}\dd{y}\dd{z} \\
            \Exp\qty(\frac{X}{X+Y+Z}) = \Exp\qty(\frac{Y}{X+Y+Z})
        \end{gather*}
        We can prove equality through a similar argument.
    \end{Proof}

    We have shown that the following statements hold for \(X, Y, Z\) positive iid random variables (either discrete r.v. or continuous r.v.):
    \begin{itemize}[$\odot$]
        \item \(\Exp\qty(\frac{A}{X + Y + Z})\) exists for all \(A \in \qty{X, Y, Z}\).
        \item $\Exp\qty(\frac{X}{X+Y+Z}) = \Exp\qty(\frac{Y}{X+Y+Z}) = \Exp\qty(\frac{Z}{X+Y+Z})$
    \end{itemize}

    \noindent Consider the following sum of expectations:
    \begin{equation*}
        2 \Exp\qty(\frac{X}{X+Y+Z}) + \Exp\qty(\frac{Z}{X+Y+Z}) = \Exp\qty(\frac{2 X + Z}{X+Y+Z})
    \end{equation*}
    Since the left side exists, we can conclude that \(\Exp\qty(\frac{2X+Z}{X+Y+Z})\) exists.

    \begin{gather*}
        3 \Exp\qty(\frac{2 X + Z}{X+Y+Z})  = \Exp\qty(\frac{2 X + Z}{X+Y+Z}) + \Exp\qty(\frac{2 Y + X}{X+Y+Z}) + \Exp\qty(\frac{2 Z + Y}{X+Y+Z}) \\
        3 \Exp\qty(\frac{2 X + Z}{X+Y+Z})  = \Exp\qty(\frac{3(X+Y+Z)}{X+Y+Z}) \\
        3 \Exp\qty(\frac{2 X + Z}{X+Y+Z})  = \Exp\qty(3) \\
        \boxed{\Exp\qty(\frac{2 X + Z}{X+Y+Z})    = 1}
    \end{gather*}

\end{solution}


\begin{exercisebox}
    \begin{exercise}[6+8 Points]
        Provide examples of discrete and continuous random variables such that their expectations do not exist.
    \end{exercise}
\end{exercisebox}

\begin{solution}
    For this problem, we have to construct a PMF (in the case of a discrete random variable) and a PDF (in the case of a continuous random variable) so that total probability exists.
    \begin{description}
        \item[\normalfont \uline{Discrete random variable:}] Consider a random variable \(X\) which takes value on \(\N\). The corresponding PMF is given by:
            \begin{equation*}
                P(X = x) :=
                \begin{cases}
                    \frac{6}{\pi^2} \cdot \frac{1}{x^2} & :~ x \in \N         \\
                    0                                   & :~ \text{otherwise}
                \end{cases}
            \end{equation*}
            This PMF satisfies the condition of total probability \ie
            \begin{equation*}
                \sum_{n \in \N} P(X = n) = \frac{6}{\pi^2} \sum_{n = 1}^{\infty} \frac{1}{n^2} = 1
            \end{equation*}
            If the following sum converges absolutely, then the expectation value of this random variable exists.
            \begin{equation*}
                \sum_{n \in \N} n P(X = n) = \frac{6}{\pi^2} \sum_{n \in \N} n \cdot \frac{1}{n^2} = \frac{6}{\pi^2} \sum_{n \in \N} \frac{1}{n}
            \end{equation*}
            We know the simplified sum diverges to \(\infty\), so the expectation of this discrete random variable does not exist.

        \item[\normalfont \uline{Continuous random variable:}] Consider a continuous random variable \(Y\) with PDF defined as follows:
            \begin{equation*}
                f_Y(y) :=
                \begin{cases}
                    0                 & :~ y < 0   \\
                    \frac{1}{(y+1)^2} & :~ y \ge 0
                \end{cases}
            \end{equation*}
            This PDF satisfies the condition of total probability \ie
            \begin{equation*}
                \int\limits_{-\infty}^{\infty} f_Y (y) \dd{y} = 1
            \end{equation*}
            also, this corresponds to a valid CDF \ie
            \begin{equation*}
                F_Y (y) := \int \limits_{-\infty}^{y} f_Y (t) \dd{t} =
                \begin{cases}
                    0             & :~ y < 0   \\
                    \frac{y}{y+1} & :~ y \ge 0
                \end{cases}
            \end{equation*}
            If the following integral converges absolutely, then the expectation value of this random variable exists.
            \begin{equation*}
                \int \limits_{-\infty}^{\infty} y \cdot f_Y(y) \dd{y} = \int \limits_{0}^{\infty} \frac{y}{(y+1)^2} \dd{y} = \lim_{a \to \infty} \int \limits_{0}^{a} \frac{y}{(y+1)^2} \dd{y} = \lim_{a \to \infty} \qty(\ln(1+a) - \frac{a}{1+a})
            \end{equation*}
            We know the last limit diverges to \(\infty\), so this continuous random variable's expectation does not exist.
    \end{description}
\end{solution}


\begin{exercisebox}
    \begin{exercise}[10 Points]
        Show that if the $n$-{th} moment exists for some $n \in \N$, then so does the $m$-th moment for all $m \in \qty{1, 2, \dots, n}$.
    \end{exercise}
\end{exercisebox}

\begin{proof}
    \uline{Discrete random variable:} Let \(n\)-th moment of a discrete random variable \(X\) exists \ie the following sum converges absolutely
    \begin{equation*}
        \sumx x^n P(X=x)
    \end{equation*}
    Consider the following sum for some \(r \in \qty{1,2, \dots, n}\),
    \begin{align*}
        \sumx \abs{x^r P(X=x)} & = \sumx \abs{x}^r P(X=x)                            \\
                               & = \underbracket[0.5pt]{\sum_{\substack{x : P(X=x)>0 \\ \abs{x} \le 1}} \abs{x}^r P(X=x)}_{S_1} + \underbracket[0.5pt]{\sum_{\substack{x : P(X=x)>0 \\ \abs{x} > 1}} \abs{x}^r P(X=x)}_{S_2}
    \end{align*}
    Consider the sum \(S_1\), we have
    \begin{gather*}
        \abs{x} \le 1 \quad \implies \quad \abs{x}^r \le 1
        \shortintertext{Using this bound on \(x\), we can bound the value of \(S_1\),}
        \abs{x}^r P(X=x) \le P(X=x) \quad \implies \quad \sum_{\substack{x : P(X=x)>0 \\ \abs{x} \le 1}} \abs{x}^r P(X=x) \le \sum_{\substack{x : P(X=x)>0 \\ \abs{x} \le 1}} P(X=x)
        \shortintertext{expanding the domain of summation gives us an upper bound of 1,}
        S_1 \le \sum_{\substack{x : P(X=x)>0 \\ \abs{x} \le 1}} P(X=x) < \sumx P(X=x) = 1 \\
        \boxed{S_1 < 1}.
    \end{gather*}
    Consider the sum \(S_2\), we have
    \begin{gather*}
        \abs{x} > 1 \quad \implies \quad \abs{x}^r \le \abs{x}^n \\
        \abs{x}^r P(X=x) \le \abs{x}^n P(X=x) \quad \implies \quad \sum_{\substack{x : P(X=x)>0 \\ \abs{x} > 1}} \abs{x}^r P(X=x) \le \sum_{\substack{x : P(X=x)>0 \\ \abs{x} > 1}} \abs{x}^n P(X=x)
        \shortintertext{expanding the domain of summation gives us an upper bound on \(S_2\),}
        S_2 \le \sum_{\substack{x : P(X=x)>0 \\ \abs{x} > 1}} \abs{x}^n P(X=x) < \sumx \abs{x}^n P(X=x)
        \shortintertext{since \(n\)-th moment exists, summation on right converges \ie \(S_2\) is finite.}
        % \boxed{S_2 < \theta_n}
        \implies \quad \sumx \abs{x^r P(X=x)} = S_1 + S_2 < \infty
    \end{gather*}
    Hence, \(r\)-th moment exists for all \(r \in \qty{1, 2, \dots, n}\). \\[1.5ex]
    % 
    \uline{Continuous random variable:} Let \(n\)-th moment of a continuous random variable \(X\) exists \ie the following integral converges absolutely
    \begin{equation*}
        \int \limits_{-\infty}^{\infty} x^n f_X (x) \dd{x}
    \end{equation*}
    Consider the following integral for some \(r \in \qty{1,2, \dots, n}\),
    \begin{align*}
        \int\limits_{-\infty}^{\infty} \abs{x}^r f_X (x) \dd{x} = \underbracket[0.5pt]{\int \limits_{-\infty}^{-1} \abs{x}^r f_X (x) \dd{x}}_{I_1} + \underbracket[0.5pt]{\int \limits_{-1}^{1} \abs{x}^r f_X (x) \dd{x}}_{I_2} + \underbracket[0.5pt]{\int \limits_{1}^{\infty} \abs{x}^r f_X (x) \dd{x}}_{I_3}
    \end{align*}
    For integrals \(I_1\) and \(I_3\) we have \(\abs{x} > 1\), so
    \begin{gather*}
        \abs{x} > 1 \quad \implies \quad \abs{x}^r \le \abs{x}^n \\
        \abs{x}^r f_X (x) \le \abs{x}^n f_X (x) \quad \implies \quad \int \limits_{-\infty}^{-1} \abs{x}^r f_X (x) \dd{x} \le \int \limits_{-\infty}^{-1} \abs{x}^n f_X (x) \dd{x} \\
        I_1 \le \int \limits_{-\infty}^{-1} \abs{x}^n f_X (x) \dd{x} < \int \limits_{-\infty}^{\infty} \abs{x}^n f_X (x) \dd{x}
        \shortintertext{Since $n$-th moment of $X$ exists, the integral on the right converges, so \(I_1\) is finite.}
        \shortintertext{Similarly,}
        \abs{x}^r f_X (x) \le \abs{x}^n f_X (x) \quad \implies \quad \int \limits_{1}^{\infty} \abs{x}^r f_X (x) \dd{x} \le \int \limits_{1}^{\infty} \abs{x}^n f_X (x) \dd{x} \\
        I_3 \le \int \limits_{1}^{\infty} \abs{x}^n f_X (x) \dd{x} < \int \limits_{-\infty}^{\infty} \abs{x}^n f_X (x) \dd{x}
    \end{gather*}
    Since $n$-th moment of $X$ exists, the integral on the right converges, so \(I_3\) is finite. \\[1.2ex]
    Consider the integral \(I_2\),
    \begin{gather*}
        \abs{x} \le 1 \quad \implies \quad \abs{x}^r \le 1 \\
        \abs{x}^r f_X (x) \le f_X (x) \quad \implies \quad \int \limits_{-1}^{1} \abs{x}^r f_X (x) \dd{x} \le \int \limits_{-1}^{1} f_X (x) \dd{x} \\
        I_2 \le \int \limits_{-1}^{1} f_X (x) \dd{x} < \int \limits_{-\infty}^{\infty} f_X (x) \dd{x} = 1
    \end{gather*}
    This shows that \(I_1, I_2,\) and \(I_3\) are finite, so \(r\)-th moment of \(X\) exists,
    \begin{equation*}
        \int\limits_{-\infty}^{\infty} \abs{x}^r f_X (x) \dd{x} = I_1 + I_2 + I_3 < \infty
    \end{equation*}
\end{proof}


\begin{exercisebox}
    \begin{exercise}[6+6+4 Points]
        Recall that for two random variables $X$ and $Y$, we define
        \begin{equation*}
            \Cov(X, Y) := \Exp(X Y) - \Exp(X) \Exp(Y).
        \end{equation*}
        \begin{enumerate}[(\textit{\roman*}), noitemsep]
            \item Show that $\Cov(X, Y) = \Exp((X - \Exp(X))(Y - \Exp(Y)))$.

            \item For random variables $X_1, X_2, \dots, X_n$ and $Y_1, Y_2, \dots, Y_m$, show that
                  \begin{equation*}
                      \Cov\qty(\sum_{i=1}^{n} a_i X_i, \sum_{j=1}^{m} b_j Y_j) = \sum_{i=1}^{n} \sum_{j=1}^{m} a_i b_j \Cov(X_i, Y_j)
                  \end{equation*}
                  for all \(a_i, b_j \in \R\).

            \item Conclude from the above that for random variables $X_1, X_2, \dots, X_n$, we have
                  \begin{equation*}
                      \Var\qty(\sum_{i=1}^{n} a_i X_i) = \sum_{i=1}^{n} a_i^2 \Var(X_i) + 2 \sum_{1 \le i < j \le n} a_i a_j \Cov(X_i, X_j)
                  \end{equation*}
                  for all \(a_i \in \R\).
        \end{enumerate}
    \end{exercise}
\end{exercisebox}

\begin{proof}
    For this exercise, we will use the linearity of expectation value.
    \begin{enumerate}[(\textit{\roman*})]
        \item Expanding RHS of the equation as \(\Exp(X Y - \Exp(X) Y - X \Exp(Y) + \Exp(X) \Exp(Y))\).
              \begin{align*}
                  \Exp((X - \Exp(X))(Y - \Exp(Y)))         & = \Exp(X Y - \Exp(X) Y - X \Exp(Y) + \Exp(X) \Exp(Y))                   \\
                                                           & = \Exp(X Y) - \Exp(\Exp(X) Y) - \Exp(X \Exp(Y)) + \Exp(\Exp(X) \Exp(Y)) \\
                                                           & = \Exp(X Y) - \Exp(X) \Exp(Y) - \Exp(X) \Exp(Y) + \Exp(X) \Exp(Y)       \\
                                                           & = \Exp(X Y) - \Exp(X) \Exp(Y)                                           \\
                  \Aboxed{\Exp((X - \Exp(X))(Y - \Exp(Y))) & = \Cov(X, Y)}
              \end{align*}

        \item Using the definition of covariance, we have
              \begin{align*}
                  \Cov\qty(\sum_{i=1}^{n} a_i X_i, \sum_{j=1}^{m} b_j Y_j)         & = \Exp\qty(\qty[\sum_{i=1}^{n} a_i X_i] \cdot \qty[\sum_{j=1}^{m} b_j Y_j]) - \Exp\qty(\sum_{i=1}^{n} a_i X_i) \Exp\qty(\sum_{j=1}^{m} b_j Y_j) \\
                                                                                   & = \Exp\qty(\sum_{i=1}^{n} \sum_{j=1}^{m} a_i b_j X_i Y_j) - \qty[\sum_{i=1}^{n} a_i \Exp(X_i)] \cdot \qty[\sum_{j=1}^{m} b_j \Exp(Y_j)]         \\
                                                                                   & = \sum_{i=1}^{n} \sum_{j=1}^{m} a_i b_j \Exp(X_i Y_j) - \sum_{i=1}^{n} \sum_{j=1}^{m} a_i b_j \Exp(X_i) \Exp(Y_j)                               \\
                                                                                   & = \sum_{i=1}^{n} \sum_{j=1}^{m} a_i b_j \qty(\Exp(X_i Y_j) - \Exp(X_i) \Exp(Y_j))                                                               \\
                  \Aboxed{\Cov\qty(\sum_{i=1}^{n} a_i X_i, \sum_{j=1}^{m} b_j Y_j) & = \sum_{i=1}^{n} \sum_{j=1}^{m} a_i b_j \Cov(X_i, Y_j)}
              \end{align*}

        \item Recall \(\Cov(X, X) = \Var(X)\) for all random variable \(X\). Now set \(n = m\), with \(a_i = b_i ~\text{and}~ X_i = Y_i  ~\text{for all}~ i \in \qty{1, 2, \dots, n}\). So we have
              \begin{equation*}
                  \Cov\qty(\sum_{i=1}^{n} a_i X_i, \sum_{j=1}^{n} b_j Y_j) = \Var\qty(\sum_{i=1}^{n} a_i X_i)
              \end{equation*}
              Using the previous result, we have,
              \begin{gather*}
                  \Var\qty(\sum_{i=1}^{n} a_i X_i) = \sum_{i=1}^{n} \sum_{j=1}^{m} a_i a_j \Cov(X_i, X_j)
                  \intertext{we can break this sum in two parts as,}
                  \Var\qty(\sum_{i=1}^{n} a_i X_i) = \sum_{\substack{1 \le i, j \le n \\ i = j}} a_i a_j \Cov(X_i, X_j) + \sum_{\substack{1 \le i, j \le n \\ i \ne j}} a_i a_j \Cov(X_i, X_j) \\
                  \Var\qty(\sum_{i=1}^{n} a_i X_i) = \sum_{i=1}^{n} a_i^2 \Var(X_i) + \sum_{1 \le i < j \le n} a_i a_j \Cov(X_i, X_j) + \sum_{1 \le j < i \le n} a_i a_j \Cov(X_i, X_j)
                  \shortintertext{Since covariance is symmetric in both the arguments, we have}
                  \boxed{\Var\qty(\sum_{i=1}^{n} a_i X_i) = \sum_{i=1}^{n} a_i^2 \Var(X_i) + 2 \sum_{1 \le i < j \le n} a_i a_j \Cov(X_i, X_j)}
              \end{gather*}
    \end{enumerate}
\end{proof}

\end{document}