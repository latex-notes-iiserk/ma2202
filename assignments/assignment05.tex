\documentclass[12pt]{scrartcl}

\usepackage[linear]{handout}
\usepackage[normalem]{ulem}
\usepackage{fouridx, mathrsfs}

\newcommand{\E}{\mathcal{E}}
\DeclareMathOperator{\Exp}{E}
\DeclareMathOperator{\Var}{Var}
\DeclareMathOperator{\Cov}{Cov}

\newcommand{\sumx}{\sum_{x: P(X=x)>0}}
\newcommand{\sumy}{\sum_{y: P(Y=y)>0}}

\graphicspath{{./images/}}

\title{Assignment 05: Solutions}
\subtitle{MA2202: Probability I}
\author{Piyush Kumar Singh \\ \texttt{22MS027} \\[-2pt] Group - B}
\date{\today}

\begin{document}
\maketitle

\begin{exercisebox}
	\begin{exercise}[4+6+8 Points]
		Prove the following statements:
		\begin{enumerate}[(a), noitemsep]
			\item Let $X$ denote the number of heads obtained when a fair coin is tossed 100 times. Show that
			\[
			P(40 < X < 60) \ge \frac{3}{4}
			\]
			\item Given $\tau > 1$, provide an example of a random variable $X$ with $\Exp(X) = \mu$ and $\Var(X) = \sigma^2$ such that
			\[
			P(\abs{X - \mu} < \tau \sigma) = 1 - \frac{1}{\tau^2}
			\]
			\item Given $\mu \in \R$, provide an example of a random variable $X$ with $\Exp(X) = \mu$ and
			\[
			P(\abs{X - \mu} < t) = 1 - \frac{1}{t^2}
			\]
			for all \(t > 1\).
		\end{enumerate}
	\end{exercise}
\end{exercisebox}

\begin{solution}
	For this exercise, we will use the following theorem,
\begin{theorem*}[$t$-sigma Rule]\label{thm:t-sigma}
	Let $X$ be a random variable with expectation $\mu$ and variance  $\sigma^2$. Then for all $t > 0$, we have
	\begin{equation}
		P(\abs{X - \mu} < t \sigma) \ge 1 - \frac{1}{t^2}
	\end{equation}
\end{theorem*}
	\begin{enumerate}[(a), noitemsep]
		\item The given random variable \(X\) is a binomial distribution, $X \sim \text{Binomial}\qty(100, \frac{1}{2})$. We know \(\Exp(X) = \mu = n p = 50\) and \(\Var(X) = \sigma^2 = n p (1-p) = 25\). Set \(t = 2\) in \nameref{thm:t-sigma}, then we have
		\begin{align*}
			P(\abs{X - 50} < 2 \cdot 5) & \ge 1 - \frac{1}{2^2} \\
			P(40 < X < 60) & \ge \frac{3}{4}.
		\end{align*}

		\item For a given \(\tau > 1\). Let us define a discrete random variable \(X\) with the following PMF
		\begin{equation*}
			P(X = x) =
			\begin{cases}
				\frac{1}{2 \tau^2} &: x = \mu - \tau \sigma \\
				1 - \frac{1}{\tau^2} &: x = \mu \\
				\frac{1}{2 \tau^2} &: x = \mu + \tau \sigma \\
			\end{cases}
		\end{equation*}
		For this random variable, we have \(\Exp(X) = \mu\) and \(\Var(X) = \sigma^2\). Consider the region \(\abs{X - \mu} < \tau \sigma\), we have
		\begin{align*}
			P(\abs{X - \mu} < \tau \sigma) & = P(\mu - \tau \sigma < X < \mu + \tau \sigma) \\
			& = P(X = \mu) \\
			P(\abs{X - \mu} < \tau \sigma) & = 1 - \frac{1}{\tau^2}
		\end{align*}

		\item Let \(\mu \in \R\) be given. Define a continuous random variable \(X\) with the following PDF
		\begin{equation*}
			f_X(x) =
			\begin{cases}
				-\frac{1}{(x - \mu)^3} &: x \le \mu - 1 \\
				0 &: \mu - 1 < x < \mu + 1 \\
				\frac{1}{(x - \mu)^3} &: x \ge \mu + 1
			\end{cases}
		\end{equation*}
		Now, we can compute the corresponding CDF for \(X\) as follows
		\begin{align*}
			F_X(y) = \int\limits_{-\infty}^{y} f_X(x) \dd{x} =
			\begin{cases}
				\frac{1}{2 (y - \mu)^2} &: y \le \mu - 1 \\[1.3ex]
				\frac{1}{2} &: \mu - 1 < y < \mu + 1 \\[1.2ex]
				1 - \frac{1}{2 (y - \mu)^2} &: y \ge \mu + 1
			\end{cases}
		\end{align*}
		We know \(P(a < X < b) = F_X(b) - F_X(a)\) for a continuous random variable (using continuity of CDF). Consider the region \(\abs{X - \mu} < t\) for any \(t > 1\), we have
		\begin{align*}
			P(\abs{X - \mu} < t) & = P(\mu - t < X < \mu + t) \\
			& = F_X(\mu + t) - F_X(\mu - t) \\
			& = \qty[1 - \frac{1}{2 ((\mu + t) - \mu)^2}] - \qty[\frac{1}{2 ((\mu - t) - \mu)^2}] \\
			& = \qty[1 - \frac{1}{2 t^2}] - \qty[\frac{1}{2 t^2}] \\
			\Aboxed{P(\abs{X - \mu} < t) & = 1 - \frac{1}{t^2}}
		\end{align*}
	\end{enumerate}
\end{solution}
\pagebreak


\begin{exercisebox}
	\begin{exercise}[10 Points]
		Let $X$ and $Y$ be jointly distributed continuous random variables such that $Y \sim N(1, 1)$. Suppose,
		\[
		\Exp(X | Y) = Y^5 - 2 Y^4 - Y^3 - Y^2.
		\]
		Determine \(\Exp(X)\).
	\end{exercise}
\end{exercisebox}

\begin{solution}
	We know for any two jointly distributed random variable \(X, ~Y\)
	\[
		\Exp(X) = \Exp(\Exp(X | Y)).
	\]
	% Since, \(\Exp(X | Y) =: f(Y)\) is a function of random variable \(Y\), so we have to compute \(\Exp(f(Y))\). 
	For that, we have to compute different moments of Normal distribution.

	\noindent We know an analytical formula for central moments of a normal distribution \(Y \sim N(\mu, \sigma^2)\).
	\begin{equation}\label{eq:central-moment-normal}
		\Exp[(Y - \mu)^n] = 
		\begin{cases}
			0 & \text{if $n$ is odd} \\
			(n-1)!! \sigma^n & \text{if $n$ is even}
		\end{cases}
	\end{equation}
	Using this analytical formula, we know,
	\begin{alignat*}{3}
		\Exp[Y] = \mu &\qquad \Exp[Y^2] = \mu^2 + \sigma^2 &\qquad \Exp[Y^3] = \mu^3 + 3 \mu \sigma^2
	\end{alignat*}
	Now we have to find the analytical form for \(4^{\text{th}}\) and \(5^{\text{th}}\) moments of \(Y\).
	\begin{itemize}[$\odot$, noitemsep]
		\item From \cref{eq:central-moment-normal} we have \(\Exp[(Y - \mu)^4] = 3 \sigma^4\).
		\begin{gather*}
			\Exp[(Y - \mu)^4] = 3 \sigma^4 \\
			\Exp[Y^4 - 4 \mu Y^3 + 6 \mu^2 Y^2 - 4 \mu^3 Y + \mu^4] = 3 \sigma^4 \\
			\Exp[Y^4] - 4 \mu \Exp[Y^3] + 6 \mu^2 \Exp[Y^2] - 4 \mu^3 \Exp[Y] + \mu^4 = 3 \sigma^4 \\
			\Exp[Y^4] - 4 \mu [\mu^3 + 3 \mu \sigma^2] + 6 \mu^2 [\mu^2 + \sigma^2] - 4 \mu^3 [\mu] + \mu^4 = 3 \sigma^4 \\
			\Exp[Y^4] - \mu^4 - 6 \mu^2 \sigma^2 = 3 \sigma^4 \\
			\boxed{\Exp[Y^4] = \mu^4 + 6 \mu^2 \sigma^2 + 3 \sigma^4}
		\end{gather*}
		
		\item From \cref{eq:central-moment-normal} we have \(\Exp[(Y - \mu)^5] = 0\).
		\begin{gather*}
			\Exp[(Y - \mu)^5] = 0 \\
			\Exp[Y^5 - 5 \mu Y^4 + 10 \mu^2 Y^3 - 10 \mu^3 Y^2 + 5 \mu^4 Y - \mu^5] = 0 \\
			\Exp[Y^5] - 5 \mu \Exp[Y^4] + 10 \mu^2 \Exp[Y^3] - 10 \mu^3 \Exp[Y^2] + 5 \mu^4 \Exp[Y] - \mu^5 = 0 \\
			\Exp[Y^5] - 5 \mu [\mu^4 + 6 \mu^2 \sigma^2 + 3 \sigma^4] + 10 \mu^2 [\mu^3 + 3 \mu \sigma^2] - 10 \mu^3 [\mu^2 + \sigma^2] + 5 \mu^4 [\mu] - \mu^5 = 0 \\
			\Exp[Y^5] - 15 \mu \sigma^4 - 10 \mu^3 \sigma^2 - \mu^5 = 0 \\
			\boxed{\Exp[Y^5] = \mu^5 + 10 \mu^3 \sigma^2 + 15 \mu \sigma^4}
 		\end{gather*}
	\end{itemize}
	In our case, we have \(\mu = 1, ~\sigma^2 = 1\). So, the corresponding moments will have the following values
	\begin{alignat*}{5}
		\Exp[Y] = 1 &\qquad \Exp[Y^2] = 2 &\qquad \Exp[Y^3] = 4 &\qquad \Exp[Y^4] = 10 &\qquad \Exp[Y^5] = 26
	\end{alignat*}
	Using these value, we can determine the value of \(\Exp[X]\) by,
	\begin{align*}
		\Exp[X] & = \Exp[\Exp[X | Y]] \\
		& = \Exp[Y^5 - 2 Y^4 - Y^3 - Y^2] \\
		& = \Exp[Y^5] - 2 \Exp[Y^4] - \Exp[Y^3] - \Exp[Y^2] \\
		& = 26 - 2 (10) - 4 - 2 \\
		\Aboxed{\Exp[X] & = 0}
	\end{align*}
\end{solution}


\begin{exercisebox}
	\begin{exercise}[8+9+10 Points]
		Prove the following statements:
		\begin{enumerate}[(a), noitemsep]
			\item Let $X_1, X_2, \dots, X_n$ be jointly distributed random variables and let $A$ denote their covariance matrix. Show that the matrix $A$ is positive semidefinite, \ie for all $\va{v} \in \R^n$,
			\[
			\va{v}\trans A \va{v} \ge 0.
			\]
			\item Using Part (a), show that no three jointly distributed Bernoulli(0.34) random variables $X, ~Y, ~Z$ satisfy the equation
			\[
				P (X = 1, Y = 1) = P (Y = 1, Z = 1) = P (Z = 1, X = 1) = 0.003.
			\]
			\item Without using Part (a), show that no three jointly distributed Bernoulli(0.34) random variables $X, ~Y, ~Z$ satisfy the above equation.
		\end{enumerate}
	\end{exercise}
\end{exercisebox}

\begin{solution}
	For this problem, we require the definition of ``Covariance Matrix.''
	\begin{definition}[Covariance Matrix]
		Let $X_1, X_2, \dots, X_n$ be jointly distributed random variables and $\mathsf{X}$ is the corresponding random vector defined by
		\begin{equation*}
			\mathsf{X} := \mqty(X_1 & X_2 & \cdots & X_n)\trans.
		\end{equation*}
		The covariance matrix \(\mathcal{C}_{\mathsf{X}}\) of these jointly distributed random variables is defined by,
		\begin{equation*}
			\mathcal{C}_{\mathsf{X}} := \biggl(\Cov[X_i, X_j]\biggr)_{i, j = 1}^{n} =
			\mqty(
				\Var[X_1] & \Cov[X_1, X_2] & \cdots & \Cov[X_1, X_n] \\
				\Cov[X_2, X_1] & \Var[X_2] & \cdots & \Cov[X_2, X_n] \\
				\vdots & \vdots & \ddots & \vdots \\
				\Cov[X_n, X_1] & \Cov[X_n, X_2] & \cdots & \Var[X_n] \\
			)
		\end{equation*}
	\end{definition}
	Considering \(\Exp[\ ]\) as a \emph{linear operator} over vector space of random variables, random vectors and random matrices, we can redefine the covariance matrix in the following manner.
\\[2ex]
	Let \(X, ~Y\) be two random variable we know that
	\begin{equation*}
		\Cov[X, Y] = \Exp[(X - \Exp(X))(Y - \Exp(Y))].
	\end{equation*}
	Say we have a random vector \(\mathsf{X} = \mqty(X_1 & X_2 & \cdots & X_n)\trans\) and we define the expectation of random vector as
	\begin{equation*}
		\Exp[\mathsf{X}] := \mqty(\Exp[X_1] & \Exp[X_2] & \cdots & \Exp[X_n])\trans \equiv \mu_{\mathsf{X}}.
	\end{equation*}
	Similarly for a random matrix \(\mathscr{X} = (X_{i j})_{i, j = 1}^{n}\) we can  define the expectation of random matrix \(\mathscr{X}\) by,
	\begin{equation*}
		\Exp[\mathscr{X}] := \biggl(\Exp[X_{i j}]\biggr)_{i, j = 1}^{n}.
	\end{equation*} 
	Using this extension we can redefine the covariance matrix \(\mathcal{C}_{\mathsf{X}}\) as the expectation value of a random matrix \(\mathscr{X}:= (\mathsf{X} - \mu_{\mathsf{X}})(\mathsf{X} - \mu_{\mathsf{X}})\trans\). After the matrix multiplication, we can observe the elements of this random matrix as
	\begin{equation*}
		\mathscr{X}_{i,j} = (X_i - \Exp[X_i])(X_j - \Exp[X_j])
	\end{equation*}
	from here we can write,
	\begin{equation*}
		\Exp[\mathscr{X}_{i,j}] = \Exp[(X_i - \Exp[X_i])(X_j - \Exp[X_j])] = \Cov[X_i, X_j] \quad\implies\quad \boxed{\mathcal{C}_{\mathsf{X}} = \Exp[\mathscr{X}] = \Exp\qty[(\mathsf{X} - \mu_{\mathsf{X}})(\mathsf{X} - \mu_{\mathsf{X}})\trans]}
	\end{equation*}

	\begin{enumerate}[(a), noitemsep]
		\item We are given that \(A\) is a covariance matrix of random variables $X_1, X_2, \dots, X_n$. From the new definition, we can write the covariance matrix \(A\) as follows
		\begin{equation*}
			A = \Exp\qty[(\mathsf{X} - \mu_{\mathsf{X}})(\mathsf{X} - \mu_{\mathsf{X}})\trans]
		\end{equation*}
		if \(\mathsf{X} = \mqty(X_1 & X_2 & \cdots & X_n)\trans\). Let \(\va{v} =\mqty(v_1 & v_2 & \cdots & v_n)\trans \in \R^n\) be an arbitrary vector,
		\begin{gather*}
			\va{v}\trans A \va{v} = \va{v}\trans \Exp\qty[(\mathsf{X} - \mu_{\mathsf{X}})(\mathsf{X} - \mu_{\mathsf{X}})\trans] \va{v} \\[1.5ex]
			\va{v}\trans A \va{v} = \mqty(v_1 & v_2 & \cdots & v_n) \mqty(
				\Var[X_1] & \Cov[X_1, X_2] & \cdots & \Cov[X_1, X_n] \\
				\Cov[X_2, X_1] & \Var[X_2] & \cdots & \Cov[X_2, X_n] \\
				\vdots & \vdots & \ddots & \vdots \\
				\Cov[X_n, X_1] & \Cov[X_n, X_2] & \cdots & \Var[X_n] \\
			) \mqty(v_1 \\ v_2 \\ \vdots \\ v_n) \\[1.5ex]
			\va{v}\trans A \va{v} = \sum_{i, j} v_i \Cov[X_i, X_j] v_j \footnotemark \\
			\va{v}\trans A \va{v} = \sum_{i, j} v_i \Exp\qty[(X_i - \Exp(X_i))(X_j - \Exp(X_j))] v_j \\
			\va{v}\trans A \va{v} = \sum_{i, j} \Exp[\qty{v_i (X_i - \Exp(X_i))} \qty{(X_j - \Exp(X_j)) v_j}]
			\shortintertext{using the linearity property of expectation operator,}
			\va{v}\trans A \va{v} = \Exp\qty[\sum_{i, j} \qty{v_i (X_i - \Exp(X_i))} \qty{(X_j - \Exp(X_j)) v_j}]
			\\% \shortintertext{since \(i, ~j\) are two independent set of indices,}
			\va{v}\trans A \va{v} = \Exp\qty[\qty{\sum_{i} v_i (X_i - \Exp(X_i))} \qty{\sum_{j} (X_j - \Exp(X_j)) v_j}]
			\shortintertext{observing the expression under the summation, we have}
			\va{v}\trans A \va{v} = \Exp\qty[\qty{\va{v}\trans (\mathsf{X} - \mu_{\mathsf{X}})} \qty{(\mathsf{X} - \mu_{\mathsf{X}})\trans \va{v}}]
			\intertext{Define a random variable \(Y:= \va{v}\trans (\mathsf{X} - \mu_{\mathsf{X}})\) and since this an inner product of two \(n\)-dimensional real vector so by the symmetry of inner product we have \(\va{v}\trans (\mathsf{X} - \mu_{\mathsf{X}}) = (\mathsf{X} - \mu_{\mathsf{X}})\trans \va{v} = Y\)}
			\va{v}\trans A \va{v} = \Exp\qty[Y^2]
			\shortintertext{since \(Y^2\) is a non-negative random variable, so the expectation of the same is non-negative,}
			\va{v}\trans A \va{v} \ge 0
		\end{gather*}\hfill $\square$
		\footnotetext{Note: the used sum is a double summation running from \(i, ~j = 1\) to \(n\) (explicitly not mentioned in the calculation).}

\begin{remark}\label{rmk:positive-det}
	Let \(\mathsf{X} = \mqty(X_1 & X_2 & \cdots & X_n)\trans\) be a \(n\)-dimensional random vector. We have shown that the corresponding covariance matrix \(\mathcal{C}_{\mathsf{X}}\) is a \textbf{symmetric} and \textbf{positive semi-definite} matrix. By ``Spectral Theorem'' there exists an orthogonal matrix \(U \in \mathcal{O}_n\) such that
	\begin{equation*}
		\mathcal{C}_{\mathsf{X}} = U\trans \Lambda U
	\end{equation*}
	where \(\Lambda = \operatorname{diag}(\lambda_1, \lambda_2, \dots, \lambda_n)\), with \(\lambda_i \in \R_{\ge 0} ~ \forall i\).
\\
	Using this representation we can have a lower bound on the \(\det(\mathcal{C}_{\mathsf{X}})\),
	\begin{equation*}
		\det(\mathcal{C}_{\mathsf{X}}) = \prod_{i = 1}^{n} \lambda_i \ge 0
	\end{equation*}
\end{remark}

		\item Suppose not there exists three jointly distributed random variables \(X_1, ~X_2, ~X_3\), with identical distribution of \(\text{Bernoulli}(0.34 =: p)\) such that
		\begin{equation*}
			P (X_i = 1, X_j = 1) = 0.003 =: a \qquad \forall(i \ne j)
		\end{equation*}

		\noindent Since \(X_i\)'s are Bernoulli random variables so \(\Exp[X_i] = p ~\forall i\) and \(\Var[X_i] = p(1-p) ~\forall i\). \\
		Define following random variables \(W_{ij} = X_i X_j \quad \forall i, j\).
		\begin{align*}
			% \shortintertext{}
			\Exp[W_{ij}] & = \sum_{m, n = 0}^{1} (m \cdot n) P(X_i = m, X_j = n) \\
			% = (0 \cdot 0) P(X_i = m, X_j = n) + (0 \cdot 1) P(X_i = 0, X_j = 1) + (1 \cdot 0) P(X_i = 1, X_j = 0) + (1 \cdot 1) P(X_i = 1, X_j = 1)
			& = (1 \cdot 1) P(X_i = 1, X_j = 1) \\
			& = a \qquad \forall i, j
		\end{align*}
		With this we can find \(\Cov[X_i, X_j] ~\forall i, j\),
		\begin{align*}
			\Cov[X_i, X_j] & = \Exp[(X_i - \Exp[X_i]) (X_j - \Exp[X_j])] \\
			& = \Exp[X_i X_j] - \Exp[X_i] \Exp[X_j] \\
			& = \Exp[W_{ij}] - (p) (p) \\
			& = a - p^2 
		\end{align*}
		For these three jointly distributed random variables we can define the covariance matrix as,
		\begin{gather*}
			\mathcal{C}_{\mathsf{X}} = \mqty(
				\Var[X_1] & \Cov[X_1, X_2] & \Cov[X_1, X_3] \\
				\Cov[X_2, X_1] & \Var[X_2] & \Cov[X_2, X_3] \\
				\Cov[X_3, X_1] & \Cov[X_3, X_2] & \Var[X_3]
			) = \mqty(
				(1-p) p & a-p^2 & a-p^2 \\
				a-p^2 & (1-p) p & a-p^2 \\
				a-p^2 & a-p^2 & (1-p) p
			)
			\shortintertext{after computing the values of each element we have,}
			\mathcal{C}_{\mathsf{X}} = \mqty(
				0.2244 & -0.1126 & -0.1126 \\
				-0.1126 & 0.2244 & -0.1126 \\
				-0.1126 & -0.1126 & 0.2244
			).
			\intertext{However, the determinant of this covariance matrix is negative,}
			\det(\mathcal{C}_{\mathsf{X}}) = -0.0000908552 < 0.
		\end{gather*}
		Recall \cref{rmk:positive-det}, so we have a contradiction. Hence we can't have three jointly distributed Bernoulli(0.34) random variables.

		\item Let's define a random variable \(T:= X_1 + X_2 + X_3\), with the help of the linearity of the expectation operator, we have
		\begin{align*}
			\Exp[T] & = \Exp[X_1 + X_2 + X_3] \\
			& = \Exp[X_1] + \Exp[X_2] + \Exp[X_3] \\
			& = p + p + p \\
			\Aboxed{\Exp[T] & = 3 p}
		\end{align*}
		Similarly, we can find the second moment of \(T\),
		\begin{gather*}
			\Exp[T^2] = \Exp\qty[(X_1 + X_2 + X_3)^2] \\
			\Exp[T^2] = \Exp[X_1^2 + X_2^2 + X_3^2 + 2 X_1 X_2 + 2 X_2 X_3 + 2 X_3 X_1] \\
			\Exp[T^2] = \Exp[X_1^2] + \Exp[X_2^2] + \Exp[X_3^2] + \Exp[2 X_1 X_2] + \Exp[2 X_2 X_3] + \Exp[2 X_3 X_1] \displaybreak[3]
			\intertext{since \(X_i\)'s are identically distributed we can write,}
			\Exp[T^2] = 3 \Exp[X_1^2] + 6 \Exp[X_1 X_2] \\
			\Exp[T^2] = 3 \Exp[X_1^2] + 6 \Exp[W_{12}] \\
			\boxed{\Exp[T^2] = 3 p + 6 a}
		\end{gather*}
		Using these computed moments we can compute the variance of the random variable \(T\),
		\begin{align*}
			\Var[T] & = \Exp[T^2] - \Exp[T]^2 \\
			& = 3 p + 6 a - 9 p^2 \\
			\Aboxed{\sigma_{T}^2 & = -0.0024 < 0}
		\end{align*}
		But we know by definition \(\sigma_{T}^2 \ge 0\), so we have a contradiction. Hence we can't have three jointly distributed Bernoulli(0.34) random variables.
	\end{enumerate}
\end{solution}

\end{document}
