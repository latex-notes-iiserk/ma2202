\documentclass[12pt]{scrartcl}

\usepackage[linear, nofancy]{handout}
\usepackage[normalem]{ulem}
\usepackage{fouridx}

\newcommand{\E}{\mathcal{E}}

\title{Assignment 01: Solutions}
\subtitle{MA2202: Probability I}
\author{Piyush Kumar Singh \\ \texttt{22MS027} \\[-2pt] Group - B}
\date{\today}

\begin{document}
\maketitle

\begin{exercisebox}
    \begin{exercise}[10 Points]
        Show that if you keep throwing a fair die, the probability of the eventual occurrence of outcome six is 1.
    \end{exercise}
\end{exercisebox}

\begin{proof}
    Since we have a fair die, then we have
    \[
        P(\qty{i}) = \frac{1}{6} \quad \forall ~ i \in \qty{1, 2, 3, 4, 5, 6}.
    \]
    In this problem, we can partition our sample space as
    \[
        \Omega = \underbrace{\qty{\text{Getting six after a finite number of rolls}}}_{A} \cup \underbrace{\qty{\text{Never getting a six in infinite rolls}}}_{B}.
    \]
    We can partition \(A\) as well with \(A_i\)'s, which are the events of getting a six at \(i^\text{th}\) roll for all \(i \in \N\) and also these partitions are pairwise disjoint so we have
    \[
        A = \bigcup_{i = 1}^{\infty} A_i. \quad \implies \quad P(A) = P\qty(\bigcup_{i = 1}^{\infty} A_i) = \sum_{i = 1}^{\infty} P(A_i)
    \]
    And observe that we have \(\boxed{P(A_i) = \frac{1}{6} \qty(\frac{5}{6})^{i-1}}\) for all \(i \in \N\).\\
    % \[
    %     P(A) = P\qty(\bigcup_{i = 1}^{n} A_i) = \sum_{i = 1}^{n} P\qty(A_i)
    % \]
    In this problem, we must find the probability of event \(A\).
    So after substituting the values of \(P\qty(A_i)\) we have
    \begin{align*}
        P(A) & = \sum_{i = 1}^{\infty} P\qty(A_i)                            \\
             & = \qty(\frac{1}{6}) \sum_{k = 0}^{\infty} \qty(\frac{5}{6})^k \\
             & = 1
    \end{align*}
    Hence, the probability of the eventual occurrence of outcome six is 1.
\end{proof}


\begin{exercisebox}
    \begin{exercise}[10 Points]
        Suppose \(\qty(\Omega, \E, P)\) be a given probability space and \(A_1, A_2, \dots, A_n \in \E\). Then show that
        \[
            P\qty(\bigcup_{i = 1}^{n} A_i) = \sum_{i = 1}^{n} P(A_i) - \sum_{1 \le i < j \le n} P(A_i \cap A_j) + \cdots + (-1)^{n-1} P\qty(\bigcap_{i = 1}^{n} A_i)
        \]
    \end{exercise}
\end{exercisebox}

\begin{proof}
    We can prove this hypothesis using induction. \\
    \uline{\textsf{Base Step}:}
    \begin{itemize}[--]
        \item For \(n = 1\), it follows trivially.
        \item For \(n = 2\), observe two equalities from set theory \ie \\
              \begin{inparaitem}[\(\odot\)]
                  \item \(A_{1} \cup A_{2} = A_{1} \cup \qty(A_{2} \cap A_{1}^{c})\) \hspace{5ex}
                  \item \(A_{2} = \qty(A_{1} \cap A_{2}) \cup \qty(A_{2} \cap A_{1}^{c})\)
              \end{inparaitem}\\
              And from these equalities, we can conclude the relation for case \(n=2\).\\
              \begin{inparaitem}[\(\odot\)]
                  \item \(P\qty(A_{1} \cup A_{2}) = P\qty(A_{1}) + P\qty(A_{2} \cap A_{1}^{c})\) \hspace{5ex}
                  \item \(P\qty(A_{2}) = P\qty(A_{1} \cap A_{2}) + P\qty(A_{2} \cap A_{1}^{c})\)
              \end{inparaitem} \\
              Combining these equations, we have
              \[
                  \boxed{P\qty(A_{1} \cup A_{2}) = P\qty(A_{1}) + P\qty(A_{2}) - P\qty(A_{1} \cap A_{2})}
              \]
    \end{itemize}
    \uline{\textsf{Induction Step}:} Let the statement be true for \(n = k\) then we have to show that statement holds for \(n = k + 1\).
    \begin{equation*}
        P\qty(\bigcup_{i = 1}^{k+1} A_i) = P\qty(\underbrace{\qty[\bigcup_{i = 1}^{k} A_i]}_{A} \cup \underbrace{A_{k+1}}_{B})
    \end{equation*}
    By using the statement for \(n = 2\) case, we have
    \begin{align*}
        P\qty(\bigcup_{i = 1}^{k+1} A_i) & = P\qty(\bigcup_{i = 1}^{k} A_i) + P\qty(A_{k+1}) - P\qty(\qty[\bigcup_{i = 1}^{k} A_i] \cap A_{k+1})                                                                                              \\
                                         & = {\color{red} P\qty(\bigcup_{i = 1}^{k} A_i)} + P\qty(A_{k+1}) - P\qty(\bigcup_{i = 1}^{k} \qty[A_i \cap A_{k+1}])
        \intertext{By the induction hypothesis, we can expand the first term as follows,}
                                         & = {\color{red} \sum_{i = 1}^{k} P(A_i) + \cdots + (-1)^{k-1} P\qty(\bigcap_{i = 1}^{k} A_i)} + P\qty(A_{k+1}) - P\qty(\bigcup_{i = 1}^{k} \qty[A_i \cap A_{k+1}])                                  \\
                                         & = \sum_{i = 1}^{k+1} P(A_i) - \sum_{1 \le i < j \le k} P(A_i \cap A_j) + \cdots + (-1)^{k-1} P\qty(\bigcap_{i = 1}^{k} A_i) - \underbracket{P\qty(\bigcup_{i = 1}^{k} \qty[A_i \cap A_{k+1}])}_{S}
    \end{align*}
    Observe term \(S\) also satisfies the induction hypothesis:
    \begin{align*}
        S = \sum_{i = 1}^{k} P\qty(A_{i} \cap A_{k+1}) + \cdots + (-1)^{k-1} P\qty(\bigcap_{i = 1}^{k+1} A_i)
    \end{align*}
    After substituting the value of \(S\) we get,
    \begin{equation*}
        \boxed{P\qty(\bigcup_{i = 1}^{k+1} A_i) = \sum_{i = 1}^{k+1} P(A_i) - \sum_{1 \le i < j \le k+1} P(A_i \cap A_j) + \cdots + (-1)^{k} P\qty(\bigcap_{i = 1}^{k+1} A_i)}
    \end{equation*}
\end{proof}


\begin{exercisebox}
    \begin{exercise}[10 Points]
        To the choice of each \(n \in \N\), could you assign a probability \(P(n) > 0\) such that the following conditions hold?
        \begin{enumerate}[(a)]
            \item \(P(m) \ne P(n)\) for all \(m, n \in \N\).
            \item The probability of choosing an odd positive integer is the same as that of choosing an even positive integer.
        \end{enumerate}
        Justify your answer!
    \end{exercise}
\end{exercisebox}

\begin{solution}
    If we have a probability map such that the probability of choosing an odd natural number is equal to the probability of choosing an even natural number, then
    \[
        P(\text{Getting an odd number}) = P(\text{Getting an even number}) = \frac{1}{2}
    \]
    Since both events are mutually exclusive and exhaustive.\\
    Let's define the probability map as follows
    \[
        P(n) =
        \begin{cases}
            \frac{2}{5^{(n+1)/2}}   & \text{if}~ n ~ \text{is odd}  \\[2ex]
            \frac{1}{2 e (n/2 -1)!} & \text{if}~ n ~ \text{is even}
        \end{cases}
    \]
    % Then clearly \(P(m) \ne P(n) ~\forall ~ m,n \in \N\) 
    Since factorial growth is much faster than exponential growth, so \(\forall ~ n>k\) for some \(k \in \N\) we have \(P(\text{odd}) > P(\text{even})\). And equality holds for a non-integer value. So, this map satisfies condition (a).

    Define, \(E := \qty{2 n ~|~ n \in \N}, O := \qty{2 n - 1 ~|~ n \in \N}\). Then
    \begin{gather*}
        P(E) = \sum_{i \in E} P(i) = \frac{1}{2 e} \sum_{j = 0}^{\infty} \frac{1}{j!} = \frac{1}{2 e} \cdot e = \frac{1}{2} \quad \qty(\text{by definition of } e) \\
        P(O) = \sum_{i \in O} P(i) = \frac{2}{5} \sum_{j = 0}^{\infty} \qty(\frac{1}{5})^j = \frac{2}{5} \cdot \frac{5}{4} = \frac{1}{2} \quad \qty(\text{by infinite GP sum})
    \end{gather*}
    Thus, this map also satisfies condition (b).
\end{solution}

\begin{exercisebox}
    \begin{exercise}[10 Points]
        Seven IISER Kolkata students participated in an event at IISER Mohali. They booked AC 3-tier tickets from Howrah to Chandigarh in Netaji Express with three AC 3-tier coaches. Every such coach has eight coupes (\ie compartments), each coupe containing eight berths. If the berths were allocated randomly, find the probability of at least two among the seven students being allocated in the same coupe.
    \end{exercise}
\end{exercisebox}

\begin{solution}
    In this problem, we have 24 coupes and 192 berths. And the total number of ways in which we can allot seven berths to students is \(\fourIdx{192}{}{}{7}{P}\). We have to find the
    \[
        P(\text{at least two students in same coupe}) = 1 - P(\text{no student in the same coupe})
    \]
    And to calculate the \(P(\text{no student in the same coupe})\), we have to find the number of ways in which we can allot berths to students in distinct coupes; the number of ways of selecting distinct coupes is \(\fourIdx{24}{}{}{7}{P}\) and the number of ways of allocating berths in these distinct coupes is \(8^7\).
    \begin{align*}
        P(\text{at least two students in same coupe}) & = 1 - P(\text{no student in same coupe})                               \\
                                                      & = 1 - \frac{\fourIdx{24}{}{}{7}{P} \cdot 8^7}{\fourIdx{192}{}{}{7}{P}} \\
                                                      & \approx 0.58
    \end{align*}
\end{solution}


\begin{exercisebox}
    \begin{exercise}[10 Points]
        Carrom is played with a red, nine black and nine white coins (and a striker) on a square board with a pocket in each corner. If all these coins are scattered randomly (none of them being in any pocket) on a 29 inch \(\times\) 29 inch carrom board, show that the probability of at least two of the coins being less than three inches apart is greater than 1/2.
    \end{exercise}
\end{exercisebox}

\begin{proof}
    We have 19 coins, and we are assuming that the coins are point objects. Now, it is enough to show the probability that at least two coins are less than three inches apart if we place 19 points on a square grid of size 29 inch \(\times\) 29 inch.

    To calculate the probability, we can divide the square carrom board into small square areas (or regions) of side 29/14 inches, which means we have divided the carrom board into 196 small squares, and these small areas will work as pigeon holes in our case. So, the total number of ways to keep 19 points in these 196 areas is \(196^{19}\).
    \[
        P(\text{atleast two coins are less than 3 inches apart}) = 1 - P(\text{all coins are greater than 3 inches apart})
    \]
    If two coins are in the same small area, then the maximum distance between them is along the diagonal \ie \((29/14) \sqrt{2} \approx 2.97 < 3\) inch. Then, to keep the distance between coins greater than 3 inches, we have to keep coins in different areas. The number of ways to place coins in different regions is \(\fourIdx{196}{}{}{19}{P}\).
    \begin{align*}
        P(\text{atleast two coins are less than 3 inches apart}) & = 1 - \frac{\fourIdx{196}{}{}{19}{P}}{196^{19}} \\
                                                                 & \approx 0.59 > \frac{1}{2}
    \end{align*}
    Hence, the probability of at least two coins being less than three inches apart is greater than 1/2.
\end{proof}
\end{document}